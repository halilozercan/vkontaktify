package com.halilibo.wekontaktify.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

public class RemoteControlReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            KeyEvent event = (KeyEvent)intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            if (KeyEvent.KEYCODE_MEDIA_PAUSE == event.getKeyCode() || KeyEvent.KEYCODE_MEDIA_PLAY == event.getKeyCode()) {
                MediaPlayerService.startPausePlay(context);
            }else if (KeyEvent.KEYCODE_MEDIA_NEXT == event.getKeyCode()) {
                MediaPlayerService.startNextSong(context);
            }else if (KeyEvent.KEYCODE_MEDIA_PREVIOUS == event.getKeyCode()) {
                MediaPlayerService.startPreviousSong(context);
            }else if (KeyEvent.KEYCODE_MEDIA_CLOSE == event.getKeyCode()) {
                MediaPlayerService.startNextSong(context);
            }
        }
    }
}