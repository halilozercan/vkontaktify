package com.halilibo.wekontaktify.Services;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by absolute on 3/5/14.
 */
public class NetworkThread extends Thread {

    private static final int POST = 0;
    private static final int GET = 1;
    private static final boolean DEBUG = false;
    NetworkCallback callback;
    int requestType;
    private HttpUriRequest post;
    private HttpUriRequest get;

    public interface NetworkCallback{
        void onSuccess(String result);
        void onFail();
        void onFailWithMessage(String resultString);
    }

    public static void startThread(String uri, NetworkCallback cb){
        NetworkThread thread = new NetworkThread(cb);
        thread.setRequest(new HttpGet(uri));
        thread.start();
    }

    public NetworkThread(NetworkCallback finishCb) {
        callback=finishCb;
        requestType=GET;
    }

    public NetworkThread setRequest(HttpUriRequest req)
    {
        if(req instanceof HttpPost)
        {
            requestType=POST;
            this.post=req;
        }
        else {
            requestType=GET;
            this.get=req;
        }
        return this;
    }

    @Override
    public void run() {
        int statusCode = 0;
        InputStream result=null;

        HttpParams httpParameters = new BasicHttpParams();


        int timeoutConnection = 20000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 0;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient client = new DefaultHttpClient(httpParameters);

        try {
            HttpResponse getResponse=null;
            if(requestType==POST){
                Log.v("Post", post.getURI().toString());
                getResponse = client.execute(post);
            }
            else {
                Log.v("Get", get.getURI().toString() );
                getResponse = client.execute(get);
            }
            statusCode = getResponse.getStatusLine().getStatusCode();

            HttpEntity getResponseEntity = getResponse.getEntity();
            result = getResponseEntity.getContent();


        }
        catch (IOException e) {
            //post.abort();
            Log.w("NetworkThread", "Error ", e);
            e.printStackTrace();
            if(e instanceof ConnectTimeoutException)
                callback.onFailWithMessage("{error:\"Bağlantı süre aşımına uğradı.\"}");
            else
                callback.onFailWithMessage("{error:\"Sunucuya ulasilamadi.\"}");

            return;
        }
        if(result==null)
        {
            callback.onFail();
            return;
        }

        StringBuilder builder=new StringBuilder();

        try {
            //BufferedReader reader = new BufferedReader();
            InputStreamReader stream = new InputStreamReader(result);
            String line;
            int r;
            char []buf = new char[1024];
            while ((r = stream.read(buf)) >= 0) {
                if(r<1024)
                    buf[r]=0;
                builder.append( buf,0,r );
                if(DEBUG){
                    String str= new String(buf).substring(0, r);
                    Log.v("", str);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
            callback.onFail();
            return;
        }

        String resultString = builder.toString();

        if (statusCode != HttpStatus.SC_OK) {
            Log.e("NetworkThread", "Error " + statusCode );
            Log.d("servererror","err "+builder.toString());
            callback.onFailWithMessage(resultString);
            return ;
        }
        callback.onSuccess(resultString);

        if(DEBUG)
        {
            try {
                File myFile = new File("/sdcard/youtube.txt");
                myFile.createNewFile();
                FileOutputStream fOut = new FileOutputStream(myFile);
                OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
                myOutWriter.write(resultString);
                myOutWriter.close();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}