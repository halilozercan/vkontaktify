package com.halilibo.wekontaktify.Services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Fragments.VkFragment;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.Model.VkParsers;
import com.halilibo.wekontaktify.R;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiAudio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class DownloadService extends Service {
    private static final String DOWNLOAD_ONE = "com.halilibo.wekontaktify.action.DOWNLOAD_ONE";
    private static final String AUDIO_TO_DOWNLOAD = "com.halilibo.wekontaktify.action.AUDIO_TO_DOWNLOAD";
    private static final String ACTION_RENEW = "com.halilibo.wekontaktify.Services.action.FOO";
    private NotificationManager mNotifyManager;
    private ArrayList<String> downloads = new ArrayList<String>();
    private NotificationCompat.Builder mBuilder;
    private DatabaseHelper db;
    private boolean busy = false;

    public DownloadService() {
    }

    public static void startDownloadOne(Context context, Audio audio) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(DOWNLOAD_ONE);
        intent.putExtra(AUDIO_TO_DOWNLOAD, audio.toJson());
        context.startService(intent);
    }

    public static void startRenew(Context context) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_RENEW);
        context.startService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.d("downloadservice", "onHandleIntent");
        db = new DatabaseHelper(this);
        if (intent != null) {
            Log.d("downloadservice","not null intent and action is " + intent.getAction());
            final String action = intent.getAction();
            if (DOWNLOAD_ONE.equals(action)) {
                Log.d("downloadservice","download one is called");
                final Audio audio = Audio.fromJson(intent.getStringExtra(AUDIO_TO_DOWNLOAD));
                if(!audio.isOffline && audio.isVk){
                    if(!db.isAudioExists(audio.song_id)){
                        db.createAudio(audio);
                    }
                    downloads.add(intent.getStringExtra(AUDIO_TO_DOWNLOAD));
                    if(!busy)
                        handleDownloadOne();
                }
            }else if (ACTION_RENEW.equals(action)) {

                handleRenew();
            }
        }

        return Service.START_STICKY;
    }

    private void handleDownloadOne() {
        if(downloads.size()>0){
            String jsonAudio = downloads.get(0);
            downloads.remove(0);
            int count;
            new AsyncTask<String, Void, Void>(){
                public int total;
                public int update;

                @Override
                protected void onPreExecute(){
                    busy = true;
                }

                @Override
                protected Void doInBackground(String... strings) {
                    int count = 0;
                    try {
                        final Audio audio = Audio.fromJson(strings[0]);
                        publishProgressOne(audio.title, 0);
                        URL url = new URL(audio.source);
                        Log.d("downloadservice", "source: " + audio.source);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lengthOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(url.openStream(), 8192);

                        // Output stream to write file
                        File folder = new File(Environment.getExternalStorageDirectory() + db.getOfflineFolder());
                        if(!folder.exists()){
                            folder.mkdir();
                        }
                        File outputFile = new File(folder, audio.title + ".mp3");
                        OutputStream output = new FileOutputStream(outputFile.getPath());

                        byte data[] = new byte[1024];

                        total = 0;
                        update = 0;

                        while ((count = input.read(data)) != -1) {
                            total += count;
                            update += count;
                            if(update >= lengthOfFile / 100) {
                                publishProgressOne(audio.title, (int) ((total * 100) / lengthOfFile));
                                update = 0;
                            }
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            // writing data to file
                            output.write(data, 0, count);
                        }
                        publishProgressOne(audio.title, 100);
                        audio.isOffline = true;
                        audio.source = outputFile.getPath();
                        new DatabaseHelper(getApplicationContext()).updateAudio(audio);

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Void voi) {
                    Intent intent = new Intent(PlaylistFragment.ACTION_PLAYLIST_FRAGMENT);
                    intent.putExtra(PlaylistFragment.PLAYLIST_UPDATE, PlaylistFragment.PLAYLIST_UPDATE);
                    sendBroadcast(intent);

                    Intent intent2 = new Intent(VkFragment.ACTION_VK_FRAGMENT);
                    intent2.putExtra(VkFragment.DOWNLOAD_DONE, VkFragment.DOWNLOAD_DONE);
                    sendBroadcast(intent2);

                    handleDownloadOne();
                }
            }.execute(jsonAudio);
        }
        else{
            busy = false;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stopForeground(true);
        }

    }

    private void publishProgressOne(String text,int percentage){
        int id = 3;
        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if(percentage > 0 && percentage < 100){
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentTitle(getResources().getString(R.string.download_in_progress))
                    .setSmallIcon(R.drawable.download)
                    .setProgress(100, percentage, false);
            if(downloads.size()>0)
                mBuilder.setContentText(text + ", " + getResources().getString(R.string.remaining) + ": " + downloads.size());
            else
                mBuilder.setContentText(text);
            // Displays the progress bar for the first time.
            startForeground(id, mBuilder.build());
        }

        else if(percentage == 100){
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentTitle(getResources().getString(R.string.download_completed))
                    .setContentText(text)
                    .setSmallIcon(R.drawable.download)
                    .setContentText(text);
            sendDownloadBroadcast();
            startForeground(id, mBuilder.build());
        }
        else if(percentage == 0){
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentTitle(getResources().getString(R.string.download_starting))
                    .setSmallIcon(R.drawable.download)
                    .setProgress(0, 0, true);
            mBuilder.setContentText(text + ", " + getResources().getString(R.string.remaining) + ": " + downloads.size());

            // Displays the progress bar for the first time.
            startForeground(id, mBuilder.build());
        }
    }

    private void sendDownloadBroadcast() {
        Intent intent = new Intent(VkFragment.ACTION_VK_FRAGMENT);
        intent.putExtra(VkFragment.NEW_DOWNLOAD, VkFragment.NEW_DOWNLOAD);
        sendBroadcast(intent);
    }

    private void handleRenew() {
        ArrayList<Audio> onlines = new DatabaseHelper(this).getOnlineAudios();
        ArrayList<String> audios = new ArrayList<String>();
        for(Audio online:onlines){
            audios.add(online.vkOwnerId + "_" + online.song_id);
        }

        String audiosQuery = audios.toString().replace("[", "").replace("]", "")
                .replace(", ", ",");
        Log.d("vkonlinedinleme", audiosQuery);
        VKRequest request = new VKRequest("audio.getById", VKParameters.from("audios", audiosQuery));
        request.setResponseParser(VkParsers.myAudioPopularParser);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d("vkonlinedinleme", "on complete: " + response.responseString);

                new DatabaseHelper(DownloadService.this).updateAllAudios(Audio.createFromVkAudios(((ArrayList<VKApiAudio>)response.parsedModel), DownloadService.this));
                Intent intent = new Intent(PlaylistFragment.ACTION_PLAYLIST_FRAGMENT);
                intent.putExtra(PlaylistFragment.PLAYLIST_UPDATE, PlaylistFragment.PLAYLIST_UPDATE);
                sendBroadcast(intent);
            }

            @Override
            public void onError(VKError error) {
                Log.d("vkonlinedinleme", "on error methodu: " + error.errorReason);
                super.onError(error);
            }
        });
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
