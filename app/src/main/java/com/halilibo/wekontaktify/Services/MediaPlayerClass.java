package com.halilibo.wekontaktify.Services;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.google.gson.Gson;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by absolute on 2/27/14.
 */
public class MediaPlayerClass {
    public static final int REPEAT_NONE = 2;
    public static final int REPEAT_ALL = 1;
    public static final int REPEAT_ONE = 0;

    public static final int EMPTY = 0;
    public static final int PLAYLIST_STOPPED = 1;
    public static final int SONG_BUFFERING = 2;
    public static final int SONG_PAUSED = 4;
    public static final int SONG_PLAYING = 5;

    public static MediaPlayerClass instance;
    private final DatabaseHelper db;
    private Context context;
    public MediaPlayer mMediaPlayer;
    public int bufferingPercent;

    public int condition = EMPTY;

    public Playlist currentPlaylist;
    public ArrayList<Integer> playSort = new ArrayList<Integer>();
    public int playingIndex;

    public boolean isShuffle = false;
    public int repeatMode = REPEAT_NONE; // 0: repeat one song, 1: repeat whole list, 2: repeat nothing

    public void resetPlayer(){
        mMediaPlayer.reset();
    }

    public void readyForPlaylist(){
        condition = EMPTY;
        currentPlaylist = null;
        playSort = null;
        playingIndex = -1;
        mMediaPlayer.reset();
    }

    public void loadPlaylist(Playlist songs, int startingPosition) {
        condition = PLAYLIST_STOPPED;
        currentPlaylist = songs;
        createPlaySort(startingPosition);
    }

    public void songBuffering(){
        condition = SONG_BUFFERING;
    }

    public void saveMediaPlayer(){
        MediaPlayerData data = new MediaPlayerData();
        data.playingIndex = playingIndex;
        data.playSort = new ArrayList<Integer>();
        data.playSort.addAll(playSort);
        data.p = currentPlaylist;
        db.saveMediaPlayer(data);
    }

    public void createPlaySort(int startingPosition) {
        playSort = new ArrayList<Integer>();
        if(isShuffle){
            for(int i=0;i<currentPlaylist.audios.size();i++){
                if(i!=startingPosition)
                    playSort.add(i);
            }
            Collections.shuffle(playSort);
            playSort.add(0, startingPosition);
        }
        else{
            for(int i=0;i<currentPlaylist.audios.size();i++){
                playSort.add(i);
            }
            Collections.rotate(playSort, playSort.size() - startingPosition);
        }
        playingIndex = 0;
    }

    /*
    * This method supposed to run at the end of playlist
    * It should reset to the starting position
    * It can be moved into service
     */
    public void stopPlaylist(int stopPosition){
        condition = PLAYLIST_STOPPED;
        playingIndex = stopPosition;
        mMediaPlayer.reset();
    }


    // Private constructor prevents instantiation from other classes
    public MediaPlayerClass(Context c) {
        mMediaPlayer = new MediaPlayer();
        this.context = c;
        this.db = new DatabaseHelper(context);
        MediaPlayerData data = db.getMediaPlayer();
        if(data!=null && data.p!=null){
            this.currentPlaylist = data.p;
            this.playSort = data.playSort;
            this.playingIndex = data.playingIndex;
            condition = PLAYLIST_STOPPED;
        }
    }

    public static ArrayList<String> AudioToStr(ArrayList<Audio> input){
        ArrayList<String> result = new ArrayList<String>();
        for(int i=0;i<input.size();i++){

            result.add(new Gson().toJson(input.get(i)));
        }
        return result;
    }

    public static ArrayList<Audio> strToAudio(ArrayList<String> input){
        ArrayList<Audio> result = new ArrayList<Audio>();
        for(int i=0;i<input.size();i++){

            result.add(new Gson().fromJson(input.get(i), Audio.class));
        }
        return result;
    }

    public Audio findPlayingSong(){
        try{
            return currentPlaylist.audios.get(playSort.get(playingIndex));
        }
        catch(Exception e){
            return currentPlaylist.audios.get(0);
        }

    }


    public void pausePlayer() {
        MediaPlayerClass.instance.mMediaPlayer.pause();
        condition = SONG_PAUSED;
    }

    public void playPlayer() {
        MediaPlayerClass.instance.mMediaPlayer.start();
        condition = SONG_PLAYING;
    }

    public boolean isLoaded() {
        if(condition == SONG_PLAYING || condition == SONG_PAUSED)
            return true;
        return false;
    }

    public void nextRepeatMode() {
        switch(repeatMode){
            case REPEAT_ALL:
                repeatMode = REPEAT_ONE;
                break;
            case REPEAT_ONE:
                repeatMode = REPEAT_NONE;
                break;
            case REPEAT_NONE:
                repeatMode = REPEAT_ALL;
                break;

        }
    }

    public void removeFromQueue(Audio audio) {

        for(int i=0;i<currentPlaylist.audios.size();i++){
            if(audio.song_id == currentPlaylist.audios.get(i).song_id){
                currentPlaylist.audios.remove(i);
                return;
            }
        }
        createPlaySort(playingIndex);
    }

    public class MediaPlayerData{
        public Playlist p;
        public ArrayList<Integer> playSort;
        public int playingIndex;

    }
}
