package com.halilibo.wekontaktify.Services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Fragments.VkFragment;
import com.halilibo.wekontaktify.MainActivity;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An {@link android.app.IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MediaPlayerService extends Service implements MediaPlayer.OnPreparedListener {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String CREATE_MEDIA_PLAYER = "com.halilibo.wekontaktify.action.CREATE_MEDIA_PLAYER";
    public static final String PAUSE_PLAY_MEDIA_PLAYER = "com.halilibo.wekontaktify.action.PAUSE_PLAY_MEDIA_PLAYER";
    public static final String SEEKBAR_UPDATED_MANUALLY = "com.halilibo.wekontaktify.action.SEEKBAR_UPDATE_MANUALLY";
    public static final String ACTION_MEDIA_PLAYER_FRAGMENT = "com.halilibo.wekontaktify.action.ACTION_MEDIA_PLAYER_FRAGMENT";
    public static final String GOTO_NEXT_SONG = "com.halilibo.wekontaktify.action.GOTO_NEXT_SONG";
    public static final String NEW_SONG_STARTED = "com.halilibo.wekontaktify.action.NEW_SONG_STARTED";
    private static final String CLOSE_MEDIA_PLAYER = "com.halilibo.wekontaktify.action.CLOSE_MEDIA_PLAYER";
    public static final String ACTION_ADD_TO_QUEUE = "com.halilibo.wekontaktify.action.ADD_TO_QUEUE";
    public static final String MEDIA_PLAYER_UPDATE = "com.halilibo.wekontaktify.action.MEDIA_PLAYER_UPDATE";
    private static final String GOTO_PREVIOUS_SONG = "com.halilibo.wekontaktify.action.GOTO_PREVIOUS_SONG";
    private static final String TOGGLE_SHUFFLE = "com.halilibo.wekontaktify.action.TOGGLE_SHUFFLE";

    private static final int NOTIFICATION_ID = 1;


    private WifiManager.WifiLock wifiLock;
    AudioManager audioManager;

    // TODO: Rename parameters
    public static final String LIST_STARTING_POSITION = "com.halilibo.wekontaktify.extra.LIST_STARTING_POSITION";
    public static final String SONGS = "com.halilibo.wekontaktify.extra.SONGS";
    public static final String MEDIA_PLAYER_DURATION = "com.halilibo.wekontaktify.extra.DURATION";
    public static final String MEDIA_PLAYER_POSITION = "com.halilibo.wekontaktify.extra.POSITION";
    public static final String PERCENT_UPDATE = "com.halilibo.wekontaktify.extra.PERCENT_UPDATED";

    private HeadsetStateReceiver headsetReceiver;
    private IntentFilter receiverFilter;
    private PendingIntent closePendingIntent;
    private Intent resultIntent, playpauseIntent, nextsongIntent, closeIntent;
    private PendingIntent resultPendingIntent;
    private PendingIntent playpausePendingIntent;
    private PendingIntent nextsongPendingIntent;
    private TimerTask sendUpdates;
    private Timer sender;
    private DatabaseHelper db;
    private Intent prevsongIntent;
    private PendingIntent prevsongPendingIntent;


    @Override
    public void onCreate(){
        super.onCreate();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        resultPendingIntent = PendingIntent.getActivity(this,1,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        playpauseIntent = new Intent(this, MediaPlayerService.class);
        playpauseIntent.setAction(MediaPlayerService.PAUSE_PLAY_MEDIA_PLAYER);
        playpauseIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        playpausePendingIntent = PendingIntent.getService(this, 1, playpauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        nextsongIntent = new Intent(this, MediaPlayerService.class);
        nextsongIntent.setAction(MediaPlayerService.GOTO_NEXT_SONG);
        nextsongIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        nextsongPendingIntent = PendingIntent.getService(this, 1, nextsongIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        prevsongIntent = new Intent(this, MediaPlayerService.class);
        prevsongIntent.setAction(MediaPlayerService.GOTO_PREVIOUS_SONG);
        prevsongIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        prevsongPendingIntent = PendingIntent.getService(this, 1, prevsongIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        closeIntent = new Intent(this, MediaPlayerService.class);
        closeIntent.setAction(MediaPlayerService.CLOSE_MEDIA_PLAYER);
        closeIntent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        closePendingIntent = PendingIntent.getService(this, 1, closeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        receiverFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        headsetReceiver = new HeadsetStateReceiver();
        registerReceiver(headsetReceiver, receiverFilter );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(headsetReceiver);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see android.app.IntentService
     */
    // TODO: Customize helper method
    public static void startPausePlay(Context context) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(PAUSE_PLAY_MEDIA_PLAYER);
        context.startService(intent);
    }

    public static void startPreviousSong(Context context) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(GOTO_PREVIOUS_SONG);
        context.startService(intent);
    }

    public static void startNextSong(Context context) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(GOTO_NEXT_SONG);
        context.startService(intent);
    }

    public static void startPlaylist(Context context, Playlist musics, int position){
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(MediaPlayerService.CREATE_MEDIA_PLAYER);
        intent.putExtra(MediaPlayerService.SONGS, musics.toJson());
        intent.putExtra(MediaPlayerService.LIST_STARTING_POSITION, position);
        context.startService(intent);
    }

    public static void startToggleShuffle(Context context){
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(TOGGLE_SHUFFLE);
        context.startService(intent);
    }

    public static void startClose(Context context){
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(CLOSE_MEDIA_PLAYER);
        context.startService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.d("mediaplayerservice","onHandleIntent");
        db = new DatabaseHelper(this);

        initializeMediaPlayer();
        MediaPlayerClass.instance.isShuffle = db.isShuffle();
        MediaPlayerClass.instance.repeatMode = db.getRepeatMode();

        if (intent != null) {
            Log.d("mediaplayerservice","not null intent and action is " + intent.getAction());
            final String action = intent.getAction();
            if (CREATE_MEDIA_PLAYER.equals(action)) {
                Log.d("mediaplayerservice","create media is called");
                final Playlist songs = Playlist.fromJson(intent.getStringExtra(SONGS));
                final int startingPosition = intent.getIntExtra(LIST_STARTING_POSITION, 0);

                handlePlaylist(songs, startingPosition);

            } else if (PAUSE_PLAY_MEDIA_PLAYER.equals(action)) {

                handlePlayPause();

            } else if (SEEKBAR_UPDATED_MANUALLY.equals(action)) {
                final int percent = intent.getIntExtra(PERCENT_UPDATE,0);

                handleSeekbarManualUpdate(percent);

            } else if (GOTO_NEXT_SONG.equals(action)) {

                handleNextSong();
            } else if (GOTO_PREVIOUS_SONG.equals(action)) {

                handlePreviousSong();
            } else if (CLOSE_MEDIA_PLAYER.equals(action)) {

                handleStopPlaylist(MediaPlayerClass.instance.playingIndex);
            }  else if(ACTION_ADD_TO_QUEUE.equals(action)){

                final ArrayList<String> songs = intent.getStringArrayListExtra(SONGS);
                handleAddToQueue(songs);
            } else if(TOGGLE_SHUFFLE.equals(action)){

                if(MediaPlayerClass.instance.currentPlaylist!=null && MediaPlayerClass.instance.currentPlaylist.audios.size()!=0)
                    MediaPlayerClass.instance.createPlaySort(MediaPlayerClass.instance.playSort.get(MediaPlayerClass.instance.playingIndex));
            }
        }
        sendUpdates = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(ACTION_MEDIA_PLAYER_FRAGMENT);
                intent.putExtra(ACTION_MEDIA_PLAYER_FRAGMENT, MEDIA_PLAYER_UPDATE);
                sendBroadcast(intent);
            }
        };
        sender = new Timer();
        sender.scheduleAtFixedRate(sendUpdates, 0, 1000);
        return Service.START_STICKY;
    }

    private void initializeMediaPlayer() {
        if(MediaPlayerClass.instance==null)
            MediaPlayerClass.instance = new MediaPlayerClass(this);
    }

    private void handleAddToQueue(ArrayList<String> songs) {
        MediaPlayerClass.instance.currentPlaylist.audios.addAll(MediaPlayerClass.strToAudio(songs));
        MediaPlayerClass.instance.createPlaySort(MediaPlayerClass.instance.playSort.get(MediaPlayerClass.instance.playingIndex));
    }

    private void handleStopPlaylist(int stopPosition) {
        MediaPlayerClass.instance.saveMediaPlayer();
        MediaPlayerClass.instance.stopPlaylist(stopPosition);
        sendNewSongLoaded();
        if(sendUpdates!=null)
            sendUpdates.run();
        stopForeground(true);
    }

    private void handleNextSong() {

        if(MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ONE){
            MediaPlayerClass.instance.repeatMode = MediaPlayerClass.REPEAT_ALL;
            db.setRepeatMode(MediaPlayerClass.instance.repeatMode);
            MediaPlayerClass.instance.playingIndex++;
            if(MediaPlayerClass.instance.playingIndex>=MediaPlayerClass.instance.playSort.size()){
                handleStopPlaylist(0);
            }
            else{
                handlePlayMusic();
            }
        }
        else if(MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ALL){
            MediaPlayerClass.instance.playingIndex++;
            if(MediaPlayerClass.instance.playingIndex>=MediaPlayerClass.instance.playSort.size()){
                MediaPlayerClass.instance.playingIndex = 0;
                handlePlayMusic();
            }
            else
                handlePlayMusic();
        }
        else{
            MediaPlayerClass.instance.playingIndex++;
            if(MediaPlayerClass.instance.playingIndex>=MediaPlayerClass.instance.playSort.size()){
                handleStopPlaylist(0);
            }
            else{
                handlePlayMusic();
            }
        }
        Log.d("nextprev", "next called");
    }

    private void handleNextSongAuto() {

        Log.d("nextsongauto", "called: " + MediaPlayerClass.instance.repeatMode + " " + MediaPlayerClass.instance.playingIndex);

        if(MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ONE){
            handlePlayMusic();
        }
        else if(MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ALL){
            MediaPlayerClass.instance.playingIndex++;
            if(MediaPlayerClass.instance.playingIndex>=MediaPlayerClass.instance.playSort.size()){
                MediaPlayerClass.instance.playingIndex = 0;
                handlePlayMusic();
            }
            else
                handlePlayMusic();
        }
        else{
            MediaPlayerClass.instance.playingIndex++;
            if(MediaPlayerClass.instance.playingIndex>=MediaPlayerClass.instance.playSort.size()){
                handleStopPlaylist(0);
            }
            else{
                handlePlayMusic();
            }
        }
        Log.d("nextprev", "next called");
    }

    private void handlePreviousSong() {
        Log.d("mediaplayer", MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition() + "");
        if(MediaPlayerClass.instance.isLoaded() && MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition() > 5000){
            handlePlayMusic();
            return;
        }
        MediaPlayerClass.instance.playingIndex--;
        if(MediaPlayerClass.instance.playingIndex < 0 ){
            //handleStopPlaylist();
            MediaPlayerClass.instance.playingIndex = MediaPlayerClass.instance.playSort.size()-1;
        }

        handlePlayMusic();
        Log.d("nextprev", "prev called");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */

    AudioManager.OnAudioFocusChangeListener mylistener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                MediaPlayerClass.instance.mMediaPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                handleStopPlaylist(MediaPlayerClass.instance.playingIndex);
                audioManager.unregisterMediaButtonEventReceiver(new ComponentName(getPackageName(), RemoteControlReceiver.class.getName()));
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (MediaPlayerClass.instance.mMediaPlayer.isPlaying()){
                    handlePlayPause();
                }

                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it'VkAlbumsObject ok to keep playing
                // at an attenuated level
                if (MediaPlayerClass.instance.mMediaPlayer.isPlaying())
                    MediaPlayerClass.instance.mMediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
        }
    };

    private void handlePlaylist(final Playlist songs, int startingPosition) {

        MediaPlayerClass.instance.readyForPlaylist();
        MediaPlayerClass.instance.loadPlaylist(songs, startingPosition);

        MediaPlayerClass.instance.mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        MediaPlayerClass.instance.mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                handleNextSongAuto();
            }
        });

        MediaPlayerClass.instance.mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                handleNextSong();
                return true;
            }
        });

        handlePlayMusic(); //play first song
    }



    public void onPrepared(MediaPlayer player) {
        MediaPlayerClass.instance.playPlayer();
        MediaPlayerClass.instance.saveMediaPlayer();

        Log.d("mediaplayerservice", "player started");
        MediaPlayerClass.instance.mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

        wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");

        wifiLock.acquire();

        Notification myNotification = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(MediaPlayerClass.instance.findPlayingSong().title)
                .setContentText(getResources().getString(R.string.playing) + MediaPlayerClass.instance.findPlayingSong().title + "\n" + MediaPlayerClass.instance.findPlayingSong().artistName)
                .setTicker(getResources().getString(R.string.now_playing) + MediaPlayerClass.instance.findPlayingSong().title)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.proto3)
                .addAction(R.drawable.previous, "", prevsongPendingIntent)
                .addAction(R.drawable.pause, "", playpausePendingIntent)
                .addAction(R.drawable.next, "", nextsongPendingIntent)
                .addAction(R.drawable.done, "", closePendingIntent)
                .build();

        startForeground(NOTIFICATION_ID, myNotification);
        /*NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        RemoteViews v=new RemoteViews(this.getPackageName(),R.layout.notification_big);
        Notification notification;
        NotificationCompat.Builder mBuilder3=new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Ujjjjj")
                .setAutoCancel(true)
                .setContentText("bujuk");
        notification=mBuilder3.build();

        v.setOnClickPendingIntent(R.id.imageView, prevsongPendingIntent);
        v.setOnClickPendingIntent(R.id.imageViewF2, playpausePendingIntent);
        v.setOnClickPendingIntent(R.id.imageView3, nextsongPendingIntent);
        v.setImageViewBitmap(R.id.imageViewF1, NotificationHelper.getAlbumArtBitmap(this));
        v.setImageViewResource(R.id.imageViewF2, R.drawable.pause);

        notification.bigContentView = v;
        Log.v("NOTIF","ICATION");
        mNotificationManager.notify(NOTIFICATION_ID, notification);*/

    }

    private void handlePlayMusic() {
        MediaPlayerClass.instance.resetPlayer();
        Audio song = MediaPlayerClass.instance.findPlayingSong();
        Log.d("mediaplayerservice", "handle play music song: " + song.title);
        Audio playableSong;
        if(db.isAudioExists(song.song_id))
            playableSong = db.getAudioFromId(song.song_id);
        else
            playableSong = song;

        try{

            MediaPlayerClass.instance.mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(playableSong.source));
            MediaPlayerClass.instance.mMediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                    MediaPlayerClass.instance.bufferingPercent = i;
                }
            });
            MediaPlayerClass.instance.mMediaPlayer.setOnPreparedListener(MediaPlayerService.this);
            MediaPlayerClass.instance.mMediaPlayer.prepareAsync(); // prepare async to not block main thread
            MediaPlayerClass.instance.songBuffering();
            sendNewSongLoaded();

        }
        catch(Exception e){
            Log.d("mediaplayerservice", "handleplaymusicerror" + e.getMessage());
        }
        audioManager.requestAudioFocus(mylistener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    private void sendNewSongLoaded() {
        Intent intent = new Intent(ACTION_MEDIA_PLAYER_FRAGMENT);
        intent.putExtra(ACTION_MEDIA_PLAYER_FRAGMENT, NEW_SONG_STARTED);
        sendBroadcast(intent);

        Intent intent2 = new Intent(PlaylistFragment.ACTION_PLAYLIST_FRAGMENT);
        intent2.putExtra(PlaylistFragment.PLAYLIST_UPDATE, PlaylistFragment.PLAYLIST_UPDATE);
        sendBroadcast(intent2);

        Intent intent3 = new Intent(VkFragment.ACTION_VK_FRAGMENT);
        intent3.putExtra(VkFragment.NEW_SONG, VkFragment.NEW_SONG);
        sendBroadcast(intent3);
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void handlePlayPause() {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if(MediaPlayerClass.instance.condition == MediaPlayerClass.PLAYLIST_STOPPED){
            handlePlayMusic();
            Log.d("mediaplayerservice", "play/pause, condition stopped");
        }
        else if(MediaPlayerClass.instance.condition == MediaPlayerClass.SONG_PAUSED) {
            MediaPlayerClass.instance.playPlayer();

            Notification myNotification = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(MediaPlayerClass.instance.findPlayingSong().title)
                    .setContentText(getResources().getString(R.string.playing) + MediaPlayerClass.instance.findPlayingSong().title + "\n" + MediaPlayerClass.instance.findPlayingSong().artistName)
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(resultPendingIntent)
                    .setSmallIcon(R.drawable.proto3)
                    .addAction(R.drawable.previous, "", prevsongPendingIntent)
                    .addAction(R.drawable.pause, "", playpausePendingIntent)
                    .addAction(R.drawable.next, "", nextsongPendingIntent)
                    .addAction(R.drawable.done, "", closePendingIntent)
                    .build();


           /* RemoteViews v=new RemoteViews(this.getPackageName(),R.layout.notification_big);
            Notification notification;
            NotificationCompat.Builder mBuilder3=new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Ujjjjj")
                    .setAutoCancel(true)
                    .setContentText("bujuk");
            notification=mBuilder3.build();

            v.setOnClickPendingIntent(R.id.imageView, prevsongPendingIntent);
            v.setOnClickPendingIntent(R.id.imageViewF2, playpausePendingIntent);
            v.setOnClickPendingIntent(R.id.imageView3, nextsongPendingIntent);

            v.setImageViewBitmap(R.id.imageViewF1, NotificationHelper.getAlbumArtBitmap(this));
            v.setImageViewResource(R.id.imageViewF2, R.drawable.play);

            notification.bigContentView = v;
            Log.v("NOTIF","ICATION");
            mNotificationManager.notify(NOTIFICATION_ID, notification);*/

            startForeground(NOTIFICATION_ID, myNotification);
        }
        else if(MediaPlayerClass.instance.condition == MediaPlayerClass.SONG_PLAYING) {
            MediaPlayerClass.instance.pausePlayer();

            Notification myNotification = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(MediaPlayerClass.instance.findPlayingSong().title)
                    .setContentText(getResources().getString(R.string.playing) + MediaPlayerClass.instance.findPlayingSong().title + "\n" + MediaPlayerClass.instance.findPlayingSong().artistName)
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(resultPendingIntent)
                    .setSmallIcon(R.drawable.proto3)
                    .addAction(R.drawable.previous, "", prevsongPendingIntent)
                    .addAction(R.drawable.play, "", playpausePendingIntent)
                    .addAction(R.drawable.next, "", nextsongPendingIntent)
                    .addAction(R.drawable.done, "", closePendingIntent)
                    .build();


           /* RemoteViews v=new RemoteViews(this.getPackageName(),R.layout.notification_big);
            Notification notification;
            NotificationCompat.Builder mBuilder3=new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Ujjjjj")
                    .setAutoCancel(true)
                    .setContentText("bujuk");
            notification=mBuilder3.build();

            v.setOnClickPendingIntent(R.id.imageView, prevsongPendingIntent);
            v.setOnClickPendingIntent(R.id.imageViewF2, playpausePendingIntent);
            v.setOnClickPendingIntent(R.id.imageView3, nextsongPendingIntent);

            v.setImageViewBitmap(R.id.imageViewF1, NotificationHelper.getAlbumArtBitmap(this));
            v.setImageViewResource(R.id.imageViewF2, R.drawable.pause);
            notification.bigContentView = v;
            Log.v("NOTIF","ICATION");
            mNotificationManager.notify(NOTIFICATION_ID, notification);*/

            startForeground(NOTIFICATION_ID, myNotification);
        }
        Intent intent = new Intent(ACTION_MEDIA_PLAYER_FRAGMENT);
        intent.putExtra(ACTION_MEDIA_PLAYER_FRAGMENT, MEDIA_PLAYER_UPDATE);
        sendBroadcast(intent);

    }

    private void handleSeekbarManualUpdate(int percent) {
        if(MediaPlayerClass.instance.mMediaPlayer!=null && MediaPlayerClass.instance.mMediaPlayer.isPlaying())
            MediaPlayerClass.instance.mMediaPlayer.seekTo(percent);
    }

    public class HeadsetStateReceiver extends BroadcastReceiver {
        public boolean isConnected;
        public HeadsetStateReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isConnectedNow = intent.getIntExtra("state",0)==1;
            Log.d("headsetconnect", isConnectedNow + " " + isConnected);

            if(isConnected!=isConnectedNow && !isConnectedNow && MediaPlayerClass.instance.mMediaPlayer!=null && MediaPlayerClass.instance.mMediaPlayer.isPlaying()){
                Intent msgIntent = new Intent(getApplicationContext(), MediaPlayerService.class);
                msgIntent.setAction(MediaPlayerService.PAUSE_PLAY_MEDIA_PLAYER);
                getApplicationContext().startService(msgIntent);
            }

            isConnected = isConnectedNow;
        }
    }

}
