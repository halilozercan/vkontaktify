package com.halilibo.wekontaktify;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.halilibo.wekontaktify.Fragments.MainMenuFragment;
import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Fragments.PlaylistsFragment;
import com.halilibo.wekontaktify.Fragments.SimpleMediaPlayerFragment;
import com.halilibo.wekontaktify.Fragments.VkFragment;
import com.halilibo.wekontaktify.Fragments.VkMenuFragment;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Prefs;
import com.halilibo.wekontaktify.Services.DownloadService;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;
import com.vk.sdk.util.VKUtil;

import net.rdrei.android.dirchooser.DirectoryChooserActivity;
import net.rdrei.android.dirchooser.DirectoryChooserFragment;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;


public class MainActivity extends Activity implements
        DirectoryChooserFragment.OnFragmentInteractionListener,
        VkMenuFragment.OnVkMenuFragmentInteractionListener,
        MainMenuFragment.OnMainMenuFragmentInteractionListener,
        VkFragment.OnVkInteractionListener,
        PlaylistsFragment.OnPlaylistsFragmentListener{

    private static final int REQUEST_DIRECTORY = 1585;
    private SimpleMediaPlayerFragment simplePlayerFragment;
    private DatabaseHelper db;
    private String[] mTitles;
    private CharSequence mTitle;
    private ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    private FragmentManager fragmentManager;
    private MainActivityReceiver myReceiver;
    private IntentFilter filter;
    private SlidingMenu menu;
    private boolean showVkMenuItem = false;
    private boolean showVkStuff = false;
    private VkMenuFragment vkMenuFragment;
    private MainMenuFragment mainMenuFragment;
    private DirectoryChooserFragment mDialog;
    private boolean multiSelect = false;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handleIntent(getIntent());

        db = new DatabaseHelper(this);

        scanAllLocalMusic();
        initializeMediaPlayer();
        initializeVkSdk();

        initializeFragments();
        initializeSideMenus();
        selectMainItem(1);


        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        simplePlayerFragment = new SimpleMediaPlayerFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.simple_media_player_container, simplePlayerFragment)
                .commit();

        myReceiver = new MainActivityReceiver();
        filter = new IntentFilter(DatabaseHelper.ACTION_DATABASE);
        registerReceiver(myReceiver, filter);

    }

    private void initializeMediaPlayer() {
        if(MediaPlayerClass.instance==null)
            MediaPlayerClass.instance = new MediaPlayerClass(this);
    }

    private void initializeSideMenus() {
        mTitles = getResources().getStringArray(R.array.navigation_menu_items);

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT_RIGHT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.setMenu(R.layout.frame_main_menu);

        menu.setSecondaryMenu(R.layout.frame_vk_menu);
        menu.setSecondaryShadowDrawable(R.drawable.shadowright);

        menu.setSlidingEnabled(false);

        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

        mainMenuFragment = new MainMenuFragment();
        vkMenuFragment = new VkMenuFragment();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_main_menu, mainMenuFragment)
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_vk_menu, vkMenuFragment)
                .commit();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            DownloadService.startRenew(this);
            return true;
        }
        return false;
    }

    private void initializeFragments() {
        fragments.add(PlaylistsFragment.newInstance(null,null));
        fragments.add(PlaylistFragment.newInstance(PlaylistFragment.ALL_TRACKS));
        fragments.add(PlaylistFragment.newInstance(PlaylistFragment.ALL_TRACKS));
        fragments.add(VkFragment.newInstance(null,null));

        fragmentManager = getFragmentManager();

        for(Fragment fragment:fragments){
            fragmentManager.beginTransaction()
                    .add(R.id.content_frame, fragment)
                    .commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        initializeMediaPlayer();
        showVkMenuItem();
        registerReceiver(myReceiver, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
        if(MediaPlayerClass.instance.condition != MediaPlayerClass.SONG_PLAYING &&
                MediaPlayerClass.instance.condition != MediaPlayerClass.SONG_BUFFERING){
            MediaPlayerService.startClose(this);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);

        if (requestCode == REQUEST_DIRECTORY) {
            if (resultCode == DirectoryChooserActivity.RESULT_CODE_DIR_SELECTED) {
                handleDirectoryChoice(data
                        .getStringExtra(DirectoryChooserActivity.RESULT_SELECTED_DIR));
            } else {
                // Nothing selected
            }
        }
    }

    private void handleDirectoryChoice(String stringExtra) {
        int start = stringExtra.lastIndexOf('/');
        stringExtra = stringExtra.substring(start);
        db.setOfflineFolder(stringExtra);
        vkMenuFragment.refreshList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if(showVkMenuItem){
            menu.getItem(1).setVisible(true);
        }
        else{
            menu.getItem(1).setVisible(false);
        }

        if(showVkStuff){
            menu.getItem(0).setVisible(true);
            menu.getItem(0).expandActionView();
        }
        else{
            menu.getItem(0).collapseActionView();
            menu.getItem(0).setVisible(false);
        }
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        // change hint color
        int searchTextViewId = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView searchTextView = (TextView) searchView.findViewById(searchTextViewId);
        searchTextView.setHintTextColor(getResources().getColor(R.color.white_color));

        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.LTGRAY);

        setSearchIcons(searchView);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }
    private void setSearchIcons(SearchView searchView){
            int closeButtonId = getResources().getIdentifier("android:id/search_close_btn", null, null);
            ImageView closeButtonImage = (ImageView) searchView.findViewById(closeButtonId);
            closeButtonImage.setImageResource(R.drawable.close);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        int id = item.getItemId();
        if(id == android.R.id.home){
            if(menu.isMenuShowing())
                menu.showContent();
            else
                menu.showMenu();
            return true;
        }
        else if(id == R.id.vk_menu){
            if(menu.isSecondaryMenuShowing())
                menu.showContent();
            else
                menu.showSecondaryMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeVkSdk() {

        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());

        VKSdk.initialize(sdkListener, Prefs.appId);
    }

    private VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
            showVkMenuItem();
            refreshMenus();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(Prefs.scope);
            refreshMenus();
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            if(authorizationError.errorCode==VKError.VK_API_ERROR){
                new AlertDialog.Builder(VKUIHelper.getTopActivity())
                        .setTitle(getResources().getString(R.string.cancelled))
                        .setMessage(getResources().getString(R.string.cancelled_auth))
                        .show();
                return;
            }
            new AlertDialog.Builder(VKUIHelper.getTopActivity())
                    .setMessage(authorizationError.toString() + "errorCode: " + authorizationError.errorCode )
                    .show();
            selectMainItem(1);
            showVkMenuItem();
            refreshMenus();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            DownloadService.startRenew(getApplicationContext());
            showVkMenuItem();
            menu.showSecondaryMenu();
            refreshMenus();
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            DownloadService.startRenew(getApplicationContext());
            showVkMenuItem();
            refreshMenus();
        }
        @Override
        public void onRenewAccessToken(VKAccessToken token) {
            showVkMenuItem();
            menu.showSecondaryMenu();
            refreshMenus();
        }
    };

    private void refreshMenus() {
        mainMenuFragment.refreshList();
        vkMenuFragment.refreshList();
    }

    public void scanAllLocalMusic(){
        ArrayList<Audio> songsList = new ArrayList<Audio>();
        String[] STAR = { "*" };

        Cursor cursor;
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";


        cursor = managedQuery(uri, STAR, selection, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Audio scanned = new Audio();
                    scanned.song_id = cursor.getInt(cursor
                            .getColumnIndex(MediaStore.Audio.Media._ID));

                    scanned.title = cursor
                            .getString(cursor
                                    .getColumnIndex(MediaStore.Audio.Media.TITLE));

                    scanned.source = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Audio.Media.DATA));

                    scanned.albumName = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Audio.Media.ALBUM));

                    scanned.albumId = cursor
                            .getInt(cursor
                                    .getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

                    scanned.artistName = cursor.getString(cursor
                            .getColumnIndex(MediaStore.Audio.Media.ARTIST));

                    scanned.artistId = cursor
                            .getInt(cursor
                                    .getColumnIndex(MediaStore.Audio.Media.ARTIST_ID));

                    scanned.duration = cursor
                            .getInt(cursor
                                    .getColumnIndex(MediaStore.Audio.Media.DURATION));

                    final Uri ART_CONTENT_URI = Uri.parse("content://media/external/audio/albumart");
                    scanned.albumArtUri = ContentUris.withAppendedId(ART_CONTENT_URI, scanned.albumId).toString();

                    scanned.isVk = false;
                    scanned.isOffline = true;

                    songsList.add(scanned);

                } while (cursor.moveToNext());
            }
        }

        db.updateAllAudios(songsList);

        //Toast.makeText(this, db.getAllAudioIds().size() + " audio has been added to database", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onVkInteraction(String string) {
        if(string.equals(VkFragment.SHOW_ACTIONS)) {
            showVkMenuItem = true;
            invalidateOptionsMenu();
        }
    }

    public void showVkMenuItem(){
        if (isOnline() && (VKSdk.wakeUpSession() || VKSdk.isLoggedIn())) {
            showVkMenuItem = true;
        }
        else{
            showVkMenuItem = false;
        }
        invalidateOptionsMenu();

    }

    public void showVkStuff(boolean is){
        showVkStuff = is;
        invalidateOptionsMenu();

    }

    /** Swaps fragments in the main content view */
    public void selectMainItem(int position) {
        showVkStuff(false);

        setTitle(mTitles[position]);
        menu.showContent();

        switch(position){
            case 0:
                showFragment(position);
                break;
            case 1:
                showFragment(position);
                break;
            /*case 2:
                return FoldersFragment.newInstance();*/
            case 3:
                if ((VKSdk.wakeUpSession() || VKSdk.isLoggedIn()) && isOnline()) {
                    menu.showSecondaryMenu();
                }
                else
                    VKSdk.authorize(Prefs.scope, true, false);
                break;

            default:
                showFragment(position);
                break;
        }
    }

    private void showFragment(int position) {
        for(int i=0;i<fragments.size();i++){
            if(i!=position)
                fragmentManager.beginTransaction().hide(fragments.get(i)).commitAllowingStateLoss();
            else
                fragmentManager.beginTransaction().show(fragments.get(i)).commitAllowingStateLoss();
        }
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            ((VkFragment)fragments.get(3)).doSearch(query);
            //use the query to search your data somehow
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    public void onPlaylistsFragment(Uri uri) {

    }

    @Override
    public void OnMenuFragmentInteraction(Fragment fragment) {

    }

    @Override
    public void OnVkMenuFragmentInteraction(Fragment fragment) {

    }

    public void selectVkItem(int position) {

        showVkStuff(false);
        showFragment(3); //VKontakte position
        menu.showContent();

        switch(position){
            case 1:
                ((VkFragment)fragments.get(3)).readyForSearch();
                setTitle(mTitles[3] + " - " + getResources().getString(R.string.search));
                showVkStuff(true);
                break;
            case 2:
                setTitle(mTitles[3] + " - " + getResources().getString(R.string.my_music));
                ((VkFragment)fragments.get(3)).getMyMusic();
                //open my music
                break;
            case 3:
                //open folders
                Toast.makeText(this, getResources().getString(R.string.will_be_implemented), Toast.LENGTH_SHORT).show();
                break;
            case 4:
                //open recommendation
                setTitle(mTitles[3] + " - " + getResources().getString(R.string.recommendations));
                ((VkFragment)fragments.get(3)).getRecommendations();
                break;
            case 5:
                //open populars
                setTitle(mTitles[3] + " - " + getResources().getString(R.string.populars));
                ((VkFragment)fragments.get(3)).getPopulars();
                break;
            case 6:
                //open downloaded songs
                setTitle(mTitles[3] + " - " + getResources().getString(R.string.downloaded_songs));
                ((VkFragment)fragments.get(3)).getDownloadedSongs();
                break;
            case 7:
                //open offline folder setting
                final Intent chooserIntent = new Intent(this, DirectoryChooserActivity.class);

                // Optional: Allow users to create a new directory with a fixed name.
                chooserIntent.putExtra(DirectoryChooserActivity.EXTRA_NEW_DIR_NAME,
                        "VKontaktify");

                // REQUEST_DIRECTORY is a constant integer to identify the request, e.g. 0
                startActivityForResult(chooserIntent, REQUEST_DIRECTORY);
                menu.showSecondaryMenu();
                break;
        }
    }

    @Override
    public void onSelectDirectory(String s) {
        int start = s.lastIndexOf('/');
        s = s.substring(start);
        db.setOfflineFolder(s);
        mDialog.dismiss();
        vkMenuFragment.refreshList();
    }

    @Override
    public void onCancelChooser() {
        mDialog.dismiss();
    }

    private class MainActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(DatabaseHelper.DATABASE_UPDATE).equals(DatabaseHelper.PLAYLISTS_UPDATE)){

                ((PlaylistsFragment)fragments.get(0)).refreshPlaylists();
            }
        }
    }


}
