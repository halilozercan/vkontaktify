package com.halilibo.wekontaktify.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.halilibo.wekontaktify.Adapters.PlaylistAdapter;
import com.halilibo.wekontaktify.Adapters.UserPlaylistsAdapter;
import com.halilibo.wekontaktify.MainActivity;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.Model.VkParsers;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.DownloadService;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiAudio;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.halilibo.wekontaktify.Fragments.VkFragment.OnVkInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VkFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class VkFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String SHOW_ACTIONS = "SHOW_ACTIONS";
    public static final String FOCUS_SEARCH = "FOCUS_SEARCH";
    public static final String NEW_SONG = "com.halilibo.wekontaktify.extra.NEW_SONG";
    public static final String ACTION_VK_FRAGMENT = "com.halilibo.wekontaktify.action.VK_FRAGMENT";
    public static final String DOWNLOAD_DONE = "com.halilibo.wekontaktify.extra.DOWNLOAD_DONE";
    public static final String VK_MULTI_SELECT = "com.halilibo.wekontaktify.extra.VK_MULTI_SELECT";
    public static final String ENTER_MULTI_SELECT_MODE = "com.halilibo.wekontaktify.action.ENTER_MULTI_SELECT_MODE";
    public static final String EXIT_MULTI_SELECT_MODE = "com.halilibo.wekontaktify.action.EXIT_MULTI_SELECT_MODE";
    public static final String NEW_DOWNLOAD = "com.halilibo.wekontaktify.extra.NEW_DOWNLOAD";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnVkInteractionListener mListener;
    private PlaylistAdapter searchAdapter;
    private DynamicListView searchListView;
    private ProgressBar loadingBar;
    private DatabaseHelper db;
    private VkReceiver myReceiver;
    private IntentFilter filter;
    private EditText filterText;
    private ImageView filterButton;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VkFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VkFragment newInstance(String param1, String param2) {
        VkFragment fragment = new VkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public VkFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_vk, container, false);

        db = new DatabaseHelper(getActivity());

        loadingBar = (ProgressBar)rootView.findViewById(R.id.vk_search_loadingbar);
        loadingBar.setVisibility(View.GONE);

        filterText = (EditText) rootView.findViewById(R.id.playlist_filter_text);
        filterButton = (ImageView) rootView.findViewById(R.id.playlist_filters_button);

        filterText.setVisibility(View.GONE);
        filterButton.setVisibility(View.GONE);

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                searchAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        filterButton.setOnClickListener(filterButtonListener);

        searchAdapter = new PlaylistAdapter(getActivity());
        searchListView = (DynamicListView) rootView.findViewById(R.id.searchlist_view);
        searchListView.setAdapter(searchAdapter);

        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                if(searchAdapter.isMultiSelect()){
                    searchAdapter.multiSelectItem(i);
                }
                else{
                    Playlist queue = new Playlist();
                    queue.title = "search";
                    queue.audios = searchAdapter.getList();
                    MediaPlayerService.startPlaylist(getActivity(), queue, i);
                }
            }
        });

        searchListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                searchAdapter.enterMultiSelectMode(position);
                return true;
            }
        });

        myReceiver = new VkReceiver();
        filter = new IntentFilter(ACTION_VK_FRAGMENT);
        getActivity().registerReceiver(myReceiver, filter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, filter);
        if(searchAdapter!=null)
            searchAdapter.notifyDataSetInvalidated();
    }

    @Override
    public void onPause(){
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnVkInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private View.OnClickListener filterButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DownloadService.startRenew(getActivity());
            PopupMenu myMenu = new PopupMenu(getActivity(), view);
            myMenu.inflate(R.menu.options_playlist_filter);
            if(searchAdapter.getOfflineFilter())
                myMenu.getMenu().removeItem(R.id.options_filter_show_offline);
            else
                myMenu.getMenu().removeItem(R.id.options_filter_show_all);

            myMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.options_filter_show_offline:
                            searchAdapter.getFilter().filter(PlaylistAdapter.ONLY_OFFLINE);
                            break;
                        case R.id.options_filter_show_all:
                            searchAdapter.getFilter().filter(PlaylistAdapter.NOT_ONLY_OFFLINE);
                            break;
                        case R.id.options_filter_sort_by_az:
                            searchAdapter.getFilter().filter(PlaylistAdapter.SORTING + 0);
                            break;
                        case R.id.options_filter_sort_by_za:
                            searchAdapter.getFilter().filter(PlaylistAdapter.SORTING + 1);
                            break;
                        case R.id.options_filter_sort_by_id:
                            searchAdapter.getFilter().filter(PlaylistAdapter.SORTING + 2);
                            break;
                    }
                    return true;
                }
            });

            myMenu.show();
        }
    };

    public void getMyMusic() {
        searchTask("MY_MUSIC", "0");
    }

    public void getRecommendations() {
        searchTask("RECOMMEN", "0");
    }

    public void getDownloadedSongs() {
        searchAdapter.source = PlaylistAdapter.DOWNLOADED_SONGS;
        searchAdapter.setList(db.getDownloadedAudios());
    }

    public void getPopulars(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_to_playlist);

        ListView lv = (ListView) dialog.findViewById(R.id.dialog_listview);

        ArrayAdapter<String> aa = new ArrayAdapter<String>(getActivity(), R.layout.genres_item_view, R.id.genres_item_text, getResources().getStringArray(R.array.genres));
        lv.setAdapter(aa);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    searchTask("ACTION_POPULAR", "-1");
                }
                else{
                    if(i>=20)
                        i++;
                    searchTask("ACTION_POPULAR", "" + i);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.setTitle(getResources().getString(R.string.populars));

        dialog.show();
    }

    public interface OnVkInteractionListener {
        // TODO: Update argument type and name
        public void onVkInteraction(String string);
    }

    public void readyForSearch(){
        mListener.onVkInteraction(VkFragment.FOCUS_SEARCH);
    }

    public void doSearch(String query){
        searchTask(query, "0");
    }

    private void searchTask(String... params){
        final String keyword = params[0];
        int genreId = Integer.parseInt(params[1]);

        publishSearchProcess((double) 0);
        VKRequest req;
        searchAdapter.source = PlaylistAdapter.VK_SEARCH;
        if(keyword.equals("ACTION_POPULAR")) {

            if(genreId == -1)
                req = new VKRequest("audio.getPopular", VKParameters.from(VKApiConst.COUNT, 30, "only_eng", 1));
            else
                req = new VKRequest("audio.getPopular", VKParameters.from(VKApiConst.COUNT, 30, "only_eng", 1, "genre_id", genreId));
            req.setResponseParser(VkParsers.myAudioPopularParser);
        }
        else if(keyword.equals("MY_MUSIC")){
            req = new VKRequest("audio.get", VKParameters.from(VKApiConst.COUNT, 20, VKApiConst.OFFSET, 0));
            req.setResponseParser(VkParsers.myAudioSearchParser);
            searchAdapter.source = PlaylistAdapter.VK_MY_MUSIC;
        }else if(keyword.equals("RECOMMEN")){
            req = new VKRequest("audio.getRecommendations", VKParameters.from(VKApiConst.COUNT, 40, VKApiConst.OFFSET, 0));
            req.setResponseParser(VkParsers.myAudioSearchParser);
        }
        else {
            req = new VKRequest("audio.search", VKParameters.from(VKApiConst.Q, keyword, "auto_complete", 1, "sort", 2, "count", 30));
            req.setResponseParser(VkParsers.myAudioSearchParser);
        }


        req.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
                publishSearchProcess((double)(bytesLoaded/bytesTotal));
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
            }

            @Override
            public void onComplete(VKResponse response) {
                publishSearchProcess((double)100);
                Log.d("searchresponse", response.responseString);

                searchAdapter.setList(Audio.createFromVkAudios((ArrayList<VKApiAudio>)response.parsedModel, getActivity()));
                filterText.setVisibility(View.VISIBLE);
                filterButton.setVisibility(View.VISIBLE);

            }
        });

    }

    private void publishSearchProcess(Double... progress){
        Log.d("onProgress", progress[0] + "");
        if(progress[0]<100){
            loadingBar.setVisibility(View.VISIBLE);
            loadingBar.setProgress(progress[0].intValue());
            loadingBar.setMax(100);
            searchListView.setVisibility(View.GONE);
        }
        else{
            loadingBar.setVisibility(View.GONE);
            searchListView.setVisibility(View.VISIBLE);
        }
        // Things to be done while execution of long running operation is in
        // progress. For example updating ProgessDialog
    }

    private class VkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(NEW_SONG)!=null && intent.getStringExtra(NEW_SONG).equals(NEW_SONG)){
                searchAdapter.notifyDataSetInvalidated();
                Log.d("newsongvk", "heard");
            }if(intent.getStringExtra(NEW_DOWNLOAD)!=null && intent.getStringExtra(NEW_DOWNLOAD).equals(NEW_DOWNLOAD)){
                if(searchAdapter.source == PlaylistAdapter.DOWNLOADED_SONGS){
                    getDownloadedSongs();
                }
            }else if(intent.getStringExtra(VK_MULTI_SELECT)!=null && intent.getStringExtra(VK_MULTI_SELECT).equals(ENTER_MULTI_SELECT_MODE)){
                filterText.setVisibility(View.INVISIBLE);
                filterButton.setImageDrawable(getResources().getDrawable(R.drawable.done));
                filterButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        searchAdapter.doneMultiSelect(filterButton);
                    }
                });
            }else if(intent.getStringExtra(VK_MULTI_SELECT)!=null && intent.getStringExtra(VK_MULTI_SELECT).equals(EXIT_MULTI_SELECT_MODE)){
                filterText.setVisibility(View.VISIBLE);
                filterButton.setImageDrawable(getResources().getDrawable(R.drawable.filter));
                filterButton.setOnClickListener(filterButtonListener);
            }else if(intent.getStringExtra(DOWNLOAD_DONE)!=null && intent.getStringExtra(DOWNLOAD_DONE).equals(DOWNLOAD_DONE)){
                searchAdapter.refreshOfflineContent();
            }
        }
    }

}
