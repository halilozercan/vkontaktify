package com.halilibo.wekontaktify.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.halilibo.wekontaktify.Adapters.MediaPlayerPagerAdapter;
import com.halilibo.wekontaktify.Adapters.PlaylistAdapter;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Use the {@link MediaPlayerFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class MediaPlayerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView playButton;
    private SeekBar seekBar;
    private TextView durationText;
    private TextView positionText;
    private ImageView playNextButton;
    private ImageView playPreButton;
    private ImageView shuffleButton;
    private ImageView repeatButton;
    private View rootView;
    private DatabaseHelper db;
    private OnMediaPlayerFragmentListener mListener;
    private ViewPager pager;
    private MediaPlayerPagerAdapter pagerAdapter;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment MediaPlayerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MediaPlayerFragment newInstance() {
        MediaPlayerFragment fragment = new MediaPlayerFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }
    public MediaPlayerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView!=null){
            ((ViewGroup)rootView.getParent()).removeView(rootView);
            return rootView;
        }
        //Start to prepare views
        rootView = inflater.inflate(R.layout.fragment_media_player, container, false);

        playButton = (ImageView)rootView.findViewById(R.id.fragment_media_player_play_button);
        playNextButton = (ImageView)rootView.findViewById(R.id.fragment_media_player_next_button);
        playPreButton = (ImageView)rootView.findViewById(R.id.fragment_media_player_pre_button);
        shuffleButton = (ImageView)rootView.findViewById(R.id.fragment_media_player_shuffle_button);
        repeatButton = (ImageView)rootView.findViewById(R.id.fragment_media_player_repeat_button);

        seekBar = (SeekBar)rootView.findViewById(R.id.fragment_media_player_seekbar);
        durationText = (TextView)rootView.findViewById(R.id.fragment_media_player_duration_text);
        positionText = (TextView)rootView.findViewById(R.id.fragment_media_player_position_text);

        if(MediaPlayerClass.instance.isShuffle)
            shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_on));
        else
            shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_off));

        if(MediaPlayerClass.instance.repeatMode==MediaPlayerClass.REPEAT_ALL)
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_on));
        else if(MediaPlayerClass.instance.repeatMode==MediaPlayerClass.REPEAT_ONE)
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_one));
        else
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_off));

        //End of preparing views
        db = new DatabaseHelper(getActivity());

        setButtonListeners();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b){
                    Intent intent = new Intent(getActivity(), MediaPlayerService.class);
                    intent.setAction(MediaPlayerService.SEEKBAR_UPDATED_MANUALLY);
                    intent.putExtra(MediaPlayerService.PERCENT_UPDATE,i);
                    getActivity().startService(intent);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if(!MediaPlayerClass.instance.mMediaPlayer.isPlaying()){
                    Intent msgIntent = new Intent(getActivity(), MediaPlayerService.class);
                    msgIntent.setAction(MediaPlayerService.PAUSE_PLAY_MEDIA_PLAYER);
                    getActivity().startService(msgIntent);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(!MediaPlayerClass.instance.mMediaPlayer.isPlaying()){
                    Intent msgIntent = new Intent(getActivity(), MediaPlayerService.class);
                    msgIntent.setAction(MediaPlayerService.PAUSE_PLAY_MEDIA_PLAYER);
                    getActivity().startService(msgIntent);
                }
            }
        });

        mediaPlayerUpdate();

        return rootView;
    }


    private void setButtonListeners() {
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent msgIntent = new Intent(getActivity(), MediaPlayerService.class);
                msgIntent.setAction(MediaPlayerService.PAUSE_PLAY_MEDIA_PLAYER);
                getActivity().startService(msgIntent);
            }
        });
        playNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent msgIntent = new Intent(getActivity(), MediaPlayerService.class);
                msgIntent.setAction(MediaPlayerService.GOTO_NEXT_SONG);
                getActivity().startService(msgIntent);
            }
        });
        playPreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerService.startPreviousSong(getActivity());
            }
        });
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerClass.instance.isShuffle = !MediaPlayerClass.instance.isShuffle;
                MediaPlayerService.startToggleShuffle(getActivity());
                db.setShuffle(MediaPlayerClass.instance.isShuffle);
                if (MediaPlayerClass.instance.isShuffle)
                    shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_on));
                else
                    shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_off));
            }
        });

        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerClass.instance.nextRepeatMode();
                db.setRepeatMode(MediaPlayerClass.instance.repeatMode);
                if (MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ONE)
                    repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_one));
                else if (MediaPlayerClass.instance.repeatMode == MediaPlayerClass.REPEAT_ALL)
                    repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_on));
                else
                    repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_off));
            }
        });
    }

    public void mediaPlayerUpdate(){
        switch(MediaPlayerClass.instance.condition){
            case MediaPlayerClass.EMPTY:
                rootView.setVisibility(View.GONE);
                getActivity().finish();
                break;
            case MediaPlayerClass.PLAYLIST_STOPPED:
                playButton.setEnabled(true);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.play));
                playNextButton.setEnabled(true);
                playPreButton.setEnabled(true);

                seekBar.setEnabled(false);
                seekBar.setMax(0);
                seekBar.setProgress(0);

                durationText.setText(milisecsToHHMMSS(0));
                positionText.setText(milisecsToHHMMSS(0));
                break;
            case MediaPlayerClass.SONG_PLAYING:
                playButton.setEnabled(true);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                playNextButton.setEnabled(true);
                playPreButton.setEnabled(true);

                seekBar.setEnabled(true);
                seekBar.setMax(MediaPlayerClass.instance.mMediaPlayer.getDuration());
                seekBar.setProgress(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition());


                durationText.setText(milisecsToHHMMSS(MediaPlayerClass.instance.mMediaPlayer.getDuration()));
                positionText.setText(milisecsToHHMMSS(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition()));
                break;
            case MediaPlayerClass.SONG_PAUSED:
                playButton.setEnabled(true);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.play));
                playNextButton.setEnabled(true);
                playPreButton.setEnabled(true);

                seekBar.setEnabled(true);
                seekBar.setMax(MediaPlayerClass.instance.mMediaPlayer.getDuration());
                seekBar.setProgress(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition());

                durationText.setText(milisecsToHHMMSS(MediaPlayerClass.instance.mMediaPlayer.getDuration()));
                positionText.setText(milisecsToHHMMSS(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition()));
                break;
            case MediaPlayerClass.SONG_BUFFERING:
                playButton.setEnabled(false);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.more));
                playNextButton.setEnabled(true);
                playPreButton.setEnabled(true);

                seekBar.setEnabled(false);
                seekBar.setSecondaryProgress(MediaPlayerClass.instance.bufferingPercent);

                durationText.setText(milisecsToHHMMSS(0));
                positionText.setText(milisecsToHHMMSS(0));
                break;
        }

        if(MediaPlayerClass.instance.isShuffle)
            shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_on));
        else
            shuffleButton.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_off));

        if(MediaPlayerClass.instance.repeatMode==MediaPlayerClass.REPEAT_ALL)
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_on));
        else if(MediaPlayerClass.instance.repeatMode==MediaPlayerClass.REPEAT_ONE)
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_one));
        else
            repeatButton.setImageDrawable(getResources().getDrawable(R.drawable.repeat_off));

    }

    private String milisecsToHHMMSS(int miliSeconds){
        int secs= miliSeconds/1000;
        int hours = secs / 3600,
                remainder = secs % 3600,
                minutes = remainder / 60,
                seconds = remainder % 60;

        String result = "";
        if(hours>=10)
            result += hours+":";
        else if(hours>0)
            result += "0" + hours + ":";
        if(minutes>=10)
            result += minutes+":";
        else if(minutes>=0)
            result += "0" + minutes + ":";
        if(seconds>=10)
            result += seconds;
        else if(seconds>=0)
            result += "0" + seconds;


        return result;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMediaPlayerFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMediaPlayerFragmentListener {
        // TODO: Update argument type and name
        public void onMediaPlayerFragmentInteraction(String s);
    }
}
