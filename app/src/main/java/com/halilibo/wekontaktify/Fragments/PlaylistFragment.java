package com.halilibo.wekontaktify.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.halilibo.wekontaktify.Adapters.PlaylistAdapter;
import com.halilibo.wekontaktify.MainActivity;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.DownloadService;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;


/**
 * Created by absolute on 3/1/14.
 */
public class PlaylistFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String ALBUM_STRING = "section_number";
    public static final String ALL_TRACKS = "ALL_TRACKS";
    public static final String PLAYLIST_UPDATE = "com.halilibo.wekontaktify.action.PLAYLIST_UPDATE";
    public static final String ACTION_PLAYLIST_FRAGMENT = "com.halilibo.wekontaktify.action.PLAYLIST_UPDATE";
    public static final String ENTER_MULTI_SELECT_MODE = "com.halilibo.wekontaktify.action.ENTER_MULTI_SELECT_MODE";
    public static final String EXIT_MULTI_SELECT_MODE = "com.halilibo.wekontaktify.action.EXIT_MULTI_SELECT_MODE";
    private Playlist playlist;
    private DynamicListView playlistView;
    private PlaylistAdapter playlistAdapter;
    private SharedPreferences mSharedPrefs;
    private String albumString;
    private DatabaseHelper db;
    private EditText filterText;
    private ImageView filterButton;
    private PlaylistReceiver myReceiver;
    private IntentFilter filter;
    private boolean multiSelectMode = false;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaylistFragment newInstance(String albumString) {

        PlaylistFragment fragment = new PlaylistFragment();
        Bundle args = new Bundle();
        args.putString(ALBUM_STRING, albumString);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaylistFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_playlist, container, false);
        playlistAdapter = new PlaylistAdapter(getActivity());
        playlistView = (DynamicListView) rootView.findViewById(R.id.playlist_view);

        filterText = (EditText) rootView.findViewById(R.id.playlist_filter_text);
        filterButton = (ImageView) rootView.findViewById(R.id.playlist_filters_button);

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                playlistAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        filterButton.setOnClickListener(filterButtonListener);

        albumString = getArguments().getString(ALBUM_STRING);

        if(albumString.equals("queue")){
            playlistView.enableDragAndDrop();
            playlistView.setOnItemLongClickListener(
                    new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(final AdapterView<?> parent, final View view,
                                                       final int position, final long id) {
                            playlistView.startDragging(position);
                            return true;
                        }
                    }
            );
        }
        else{
            playlistView.setOnItemLongClickListener(
                    new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(final AdapterView<?> parent, final View view,
                                                       final int position, final long id) {
                            playlistAdapter.enterMultiSelectMode(position);
                            return true;
                        }
                    }
            );
        }

        db = new DatabaseHelper(getActivity().getApplicationContext());
        mSharedPrefs = db.getPrefs();

        playlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                if(playlistAdapter.isMultiSelect()){
                    playlistAdapter.multiSelectItem(i);
                }
                else{
                    Playlist queue = new Playlist();
                    queue.title = playlist.title;
                    queue.audios = playlistAdapter.getList();
                    playCurrentList(i, queue);
                }
            }

        });
        getAudiosFromPlaylist();
        playlistView.setAdapter(playlistAdapter);

        myReceiver = new PlaylistReceiver();
        filter = new IntentFilter(ACTION_PLAYLIST_FRAGMENT);
        getActivity().registerReceiver(myReceiver, filter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(myReceiver, filter);
        playlistAdapter.notifyDataSetChanged();
        if(playlistAdapter.source == playlistAdapter.QUEUE){
            scrollToPlaying();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    private View.OnClickListener filterButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DownloadService.startRenew(getActivity());
            PopupMenu myMenu = new PopupMenu(getActivity(), view);
            myMenu.inflate(R.menu.options_playlist_filter);
            if(playlistAdapter.getOfflineFilter())
                myMenu.getMenu().removeItem(R.id.options_filter_show_offline);
            else
                myMenu.getMenu().removeItem(R.id.options_filter_show_all);

            myMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.options_filter_show_offline:
                            playlistAdapter.getFilter().filter(PlaylistAdapter.ONLY_OFFLINE);
                            break;
                        case R.id.options_filter_show_all:
                            playlistAdapter.getFilter().filter(PlaylistAdapter.NOT_ONLY_OFFLINE);
                            break;
                        case R.id.options_filter_sort_by_az:
                            playlistAdapter.getFilter().filter(PlaylistAdapter.SORTING + 0);
                            break;
                        case R.id.options_filter_sort_by_za:
                            playlistAdapter.getFilter().filter(PlaylistAdapter.SORTING + 1);
                            break;
                        case R.id.options_filter_sort_by_id:
                            playlistAdapter.getFilter().filter(PlaylistAdapter.SORTING + 2);
                            break;
                    }
                    return true;
                }
            });

            myMenu.show();
        }
    };

    private void playCurrentList(int i, Playlist p) {

        MediaPlayerService.startPlaylist(getActivity(), p, i);
    }

    private void getAudiosFromPlaylist(){
        if(albumString.equals(ALL_TRACKS)){
            playlist = db.getAllAudiosPlaylist();
            playlistAdapter.source = PlaylistAdapter.ALL_TRACKS;
        }
        else if(albumString.equals("queue")){
            playlist = MediaPlayerClass.instance.currentPlaylist;
            playlistAdapter.source = PlaylistAdapter.QUEUE;
        }
        else {
            playlist = db.getPlaylist(albumString);
            playlistAdapter.source = PlaylistAdapter.PLAYLIST;
            playlistAdapter.playlistTitle = albumString;
        }

        playlistAdapter.setList(playlist.audios);

    }
/*
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            scrollToPlaying();
        }
    }*/

    public void scrollToPlaying() {
        int pos = 0;
        for(int i=0; i<playlist.audios.size(); i++){
            if(playlist.audios.get(i).song_id == MediaPlayerClass.instance.findPlayingSong().song_id){
                pos = i;
                break;
            }
        }
        if(pos>=1) pos--;
        playlistView.setSelection(pos);
        /*
        final int pos2 = pos;
        new Thread(new Runnable() {

            @Override
            public void run() {
                playlistView.smoothScrollToPositionFromTop(pos2, 1, 1000);

            }
        }).start();*/
    }

    private class PlaylistReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(PLAYLIST_UPDATE).equals(PLAYLIST_UPDATE)){
                getAudiosFromPlaylist();
            }
            else if(intent.getStringExtra(PLAYLIST_UPDATE).equals(ENTER_MULTI_SELECT_MODE)){
                filterText.setVisibility(View.INVISIBLE);
                filterButton.setImageDrawable(getResources().getDrawable(R.drawable.done));
                filterButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    playlistAdapter.doneMultiSelect(filterButton);
                    }
                });
            }
            else if(intent.getStringExtra(PLAYLIST_UPDATE).equals(EXIT_MULTI_SELECT_MODE)){
                filterText.setVisibility(View.VISIBLE);
                filterButton.setImageDrawable(getResources().getDrawable(R.drawable.filter));
                filterButton.setOnClickListener(filterButtonListener);
            }
        }
    }
}