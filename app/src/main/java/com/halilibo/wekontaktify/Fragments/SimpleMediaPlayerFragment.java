package com.halilibo.wekontaktify.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.halilibo.wekontaktify.MediaPlayerActivity;
import com.halilibo.wekontaktify.Model.NotificationHelper;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SimpleMediaPlayerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SimpleMediaPlayerFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SimpleMediaPlayerFragment extends Fragment {

    private ImageView playButton;
    private TextView titleText;
    private TextView artistText;
    private ImageView playNextButton;
    private ToggleButton shuffleButton;
    private View rootView;
    private ProgressBar progressBar;
    private MediaPlayerReceiver myReceiver;
    private IntentFilter filter;
    private ImageView playPreButton;
    private Drawable yourDrawable;
    private ImageView albumArt;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SimpleMediaPlayerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SimpleMediaPlayerFragment newInstance() {
        SimpleMediaPlayerFragment fragment = new SimpleMediaPlayerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public SimpleMediaPlayerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myReceiver = new MediaPlayerReceiver();
        filter = new IntentFilter(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT);
        getActivity().registerReceiver(myReceiver, filter);
    }

    @Override
    public View getView(){
        return rootView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(rootView!=null){
            ((ViewGroup)rootView.getParent()).removeView(rootView);
            return rootView;
        }

        rootView = inflater.inflate(R.layout.fragment_simple_media_player, container, false);

        playButton = (ImageView)rootView.findViewById(R.id.media_player_play_button);
        playNextButton = (ImageView)rootView.findViewById(R.id.play_next_song_button);
        playPreButton = (ImageView)rootView.findViewById(R.id.play_previous_song_button);

        albumArt = (ImageView)rootView.findViewById(R.id.media_player_album_art);

        progressBar = (ProgressBar)rootView.findViewById(R.id.media_player_progressbar);
        titleText = (TextView)rootView.findViewById(R.id.media_player_title_text);
        artistText = (TextView)rootView.findViewById(R.id.media_player_artist_text);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerService.startPausePlay(getActivity());
            }
        });
        playNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerService.startNextSong(getActivity());
            }
        });
        playPreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayerService.startPreviousSong(getActivity());
            }
        });

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().registerReceiver(myReceiver, filter);
        newSong();
        mediaPlayerUpdate();
    }

    @Override
    public void onPause(){
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void setViewCondition(boolean enablePlay, boolean enableNextPre, int visibility){

        playButton.setEnabled(enablePlay);
        playNextButton.setEnabled(enableNextPre);
        playPreButton.setEnabled(enableNextPre);

        playNextButton.setVisibility(visibility);
        titleText.setVisibility(visibility);
        artistText.setVisibility(visibility);
        rootView.setVisibility(visibility);
    }

    public void mediaPlayerUpdate(){

        switch(MediaPlayerClass.instance.condition){
            case MediaPlayerClass.EMPTY:
                rootView.setVisibility(View.GONE);
                break;
            case MediaPlayerClass.PLAYLIST_STOPPED:
                setViewCondition(true, true, View.VISIBLE);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.play));

                progressBar.setMax(100);
                progressBar.setProgress(0);

                titleText.setText(MediaPlayerClass.instance.findPlayingSong().title);
                artistText.setText(MediaPlayerClass.instance.findPlayingSong().artistName);
                break;
            case MediaPlayerClass.SONG_PLAYING:
                setViewCondition(true, true, View.VISIBLE);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.pause));

                progressBar.setMax(MediaPlayerClass.instance.mMediaPlayer.getDuration());
                progressBar.setProgress(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition());

                titleText.setText(MediaPlayerClass.instance.findPlayingSong().title);
                artistText.setText(MediaPlayerClass.instance.findPlayingSong().artistName);
                break;
            case MediaPlayerClass.SONG_PAUSED:
                setViewCondition(true, true, View.VISIBLE);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.play));

                progressBar.setMax(MediaPlayerClass.instance.mMediaPlayer.getDuration());
                progressBar.setProgress(MediaPlayerClass.instance.mMediaPlayer.getCurrentPosition());

                titleText.setText(MediaPlayerClass.instance.findPlayingSong().title);
                artistText.setText(MediaPlayerClass.instance.findPlayingSong().artistName);
                break;
            case MediaPlayerClass.SONG_BUFFERING:
                setViewCondition(false, true, View.VISIBLE);
                playButton.setImageDrawable(getResources().getDrawable(R.drawable.more));

                titleText.setText(MediaPlayerClass.instance.findPlayingSong().title);
                artistText.setText(MediaPlayerClass.instance.findPlayingSong().artistName);
                break;

        }
    }

    private class MediaPlayerReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT).equals(MediaPlayerService.MEDIA_PLAYER_UPDATE)){
                mediaPlayerUpdate();
            }else if(intent.getStringExtra(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT).equals(MediaPlayerService.NEW_SONG_STARTED)) {
                newSong();
            }
        }
    }

    private void newSong() {
        //if(MediaPlayerClass.instance.isLoaded()){

            yourDrawable = NotificationHelper.getAlbumArtDrawable(getActivity());
            albumArt.setMaxWidth(albumArt.getHeight());
            albumArt.setImageDrawable(yourDrawable);
        //}

    }

}
