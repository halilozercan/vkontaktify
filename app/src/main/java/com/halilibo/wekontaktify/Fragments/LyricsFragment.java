package com.halilibo.wekontaktify.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.halilibo.wekontaktify.Services.NetworkThread;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.halilibo.wekontaktify.Fragments.LyricsFragment.OnLyricsFragmentListener} interface
 * to handle interaction events.
 * Use the {@link LyricsFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class LyricsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String SONG_ID = "SONG_ID";

    // TODO: Rename and change types of parameters
    private int songId;

    private OnLyricsFragmentListener mListener;
    private TextView lyricsTextView;
    private DatabaseHelper db;
    private MediaPlayerReceiver myReceiver;
    private IntentFilter filter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment LyricsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LyricsFragment newInstance(int param1) {
        LyricsFragment fragment = new LyricsFragment();
        Bundle args = new Bundle();
        args.putInt(SONG_ID, param1);
        fragment.setArguments(args);
        return fragment;
    }
    public LyricsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            songId = getArguments().getInt(SONG_ID);
        }
        db = new DatabaseHelper(getActivity());
        myReceiver = new MediaPlayerReceiver();
        filter = new IntentFilter(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT);
        getActivity().registerReceiver(myReceiver, filter);
    }


    @Override
    public void onResume(){
        super.onResume();
        getActivity().registerReceiver(myReceiver, filter);
        newSong();
    }

    @Override
    public void onPause(){
        super.onPause();
        getActivity().unregisterReceiver(myReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lyrics, container, false);

        lyricsTextView  = (TextView) rootView.findViewById(R.id.fragment_lyrics_textview);

        if(songId!=0)
            getLyrics(MediaPlayerClass.instance.findPlayingSong());

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnLyricsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void newSong() {
        getLyrics(MediaPlayerClass.instance.findPlayingSong());
    }

    public interface OnLyricsFragmentListener {
        // TODO: Update argument type and name
        public void onLyricsFragmentInteraction(String s);
    }


    private void getLyricsWeb(final Audio audio){
        try {
            if(audio.title.length()<10)
                audio.title += " " + audio.artistName;
            audio.title = audio.title.replaceAll("\\(.*?\\)", "");
            NetworkThread.startThread("http://halilibo.com/lyrics.php?q=" + URLEncoder.encode(audio.title, "utf-8") + "&lucky=1", new NetworkThread.NetworkCallback() {
                @Override
                public void onSuccess(final String result) {
                    if(getActivity()==null)
                        if (result.length() > 50)
                            db.updateLyrics(audio, result);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lyricsTextView.setText(Html.fromHtml(result));
                            if (result.length() > 50)
                                db.updateLyrics(audio, result);
                        }
                    });
                }

                @Override
                public void onFail() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lyricsTextView.setText(getResources().getString(R.string.error_occurred));
                        }
                    });
                }

                @Override
                public void onFailWithMessage(final String resultString) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lyricsTextView.setText(getResources().getString(R.string.error_occurred) + " " + resultString);
                        }
                    });
                }
            });
            lyricsTextView.setText(getResources().getString(R.string.loading));
            Log.d("lyricsweb", "http://halilibo.com/lyrics.php?q=" + URLEncoder.encode(audio.title + " lyrics", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void getLyricsVk(final Audio audio) {
        int lyrics_id = audio.lyrics_id;
        if(!audio.isVk){
            lyricsTextView.setText(getResources().getString(R.string.not_vk_no_lyrics));
            return;
        }
        else if(lyrics_id == 0){
            lyricsTextView.setText(getResources().getString(R.string.no_lyrics_found));
            return;
        }
        VKRequest req = new VKRequest("audio.getLyrics", VKParameters.from("lyrics_id", lyrics_id));
        lyricsTextView.setText(getResources().getString(R.string.loading));
        req.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d("getlyrics", "response: " + response.responseString);
                try {
                    lyricsTextView.setText(response.json.getJSONObject("response").getString("text"));
                    db.updateLyrics(audio, response.json.getJSONObject("response").getString("text"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VKError error) {
                Log.d("getlyrics", "response: error");
                lyricsTextView.setText(getResources().getString(R.string.error_occurred));
                super.onError(error);
            }
        });
    }

    private void getLyrics(Audio audio){

        if(!db.getLyrics(audio).equals("")){
            lyricsTextView.setText(Html.fromHtml(db.getLyrics(audio)));
        }
        else {
            lyricsTextView.setText(getResources().getString(R.string.find_lyrics_with_buttons));
        }
    }

    public void searchLyricsVk(){
        getLyricsVk(MediaPlayerClass.instance.findPlayingSong());
    }

    public void searchLyricsWeb(){
        getLyricsWeb(MediaPlayerClass.instance.findPlayingSong());
    }

    private class MediaPlayerReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT).equals(MediaPlayerService.NEW_SONG_STARTED)){
                getLyrics(MediaPlayerClass.instance.findPlayingSong());
            }
        }
    }

}
