package com.halilibo.wekontaktify;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.halilibo.wekontaktify.Adapters.MediaPlayerPagerAdapter;
import com.halilibo.wekontaktify.Fragments.LyricsFragment;
import com.halilibo.wekontaktify.Fragments.MediaPlayerFragment;
import com.halilibo.wekontaktify.Model.NotificationHelper;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;


public class MediaPlayerActivity extends Activity implements
        MediaPlayerFragment.OnMediaPlayerFragmentListener,
        LyricsFragment.OnLyricsFragmentListener{

    private MediaPlayerFragment mPlayerFragment;
    private MediaPlayerReceiver myReceiver;
    private IntentFilter filter;
    private ViewPager pager;
    private MediaPlayerPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        mPlayerFragment = new MediaPlayerFragment();

        pager = (ViewPager) findViewById(R.id.media_player_pager);
        pagerAdapter = new MediaPlayerPagerAdapter(this, getFragmentManager());
        pager.setAdapter(pagerAdapter);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.media_player_container, mPlayerFragment)
                    .commit();
        }

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.media_player_pager_tabs);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
                pagerAdapter.selected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        myReceiver = new MediaPlayerReceiver();
        filter = new IntentFilter(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT);
        registerReceiver(myReceiver, filter);
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(myReceiver, filter);
        newSong();
    }

    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(myReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.media_player, menu);
        //LyricsFragment
        if(pager.getCurrentItem() == 0 ){
            if(!MediaPlayerClass.instance.findPlayingSong().isVk){
                menu.getItem(0).setVisible(false);
            }
        }
        //QueueFragment
        else if(pager.getCurrentItem() == 1){
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }else if(id == R.id.search_vk){
            pagerAdapter.searchLyricsVk();
        }else if(id == R.id.search_web){
            pagerAdapter.searchLyricsWeb();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMediaPlayerFragmentInteraction(String s) {

    }

    @Override
    public void onLyricsFragmentInteraction(String s) {

    }

    private class MediaPlayerReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT).equals(MediaPlayerService.NEW_SONG_STARTED)){
                newSong();
            }else if(intent.getStringExtra(MediaPlayerService.ACTION_MEDIA_PLAYER_FRAGMENT).equals(MediaPlayerService.MEDIA_PLAYER_UPDATE)){
                mPlayerFragment.mediaPlayerUpdate();
            }
        }
    }

    public void newSong() {

        setTitle(MediaPlayerClass.instance.findPlayingSong().title + " - " + MediaPlayerClass.instance.findPlayingSong().artistName);

        Drawable yourDrawable = NotificationHelper.getAlbumArtDrawable(this);
        getActionBar().setIcon(yourDrawable);
    }
}
