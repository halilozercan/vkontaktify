package com.halilibo.wekontaktify;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Fragments.SimpleMediaPlayerFragment;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.PlaylistDataObject;
import com.vk.sdk.VKUIHelper;


public class PlaylistActivity extends Activity {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private PlaylistFragment currentFragment;
    private String albumString;
    private DatabaseHelper db;
    private PlaylistDataObject dataPlaylist;
    private SimpleMediaPlayerFragment mPlayerFragment;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        db = new DatabaseHelper(this);
        mPlayerFragment = new SimpleMediaPlayerFragment();
        // Set up the action bar to show a dropdown list.
        actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(true);

        albumString = getIntent().getStringExtra(PlaylistFragment.ALBUM_STRING);
        dataPlaylist = db.getPlaylistDataObject(albumString);
        actionBar.setTitle(dataPlaylist.title);

        currentFragment = PlaylistFragment.newInstance(albumString);
        getFragmentManager().beginTransaction()
                .replace(R.id.playlist_container, currentFragment)
                .commit();

        getFragmentManager().beginTransaction()
                .replace(R.id.media_player_container, mPlayerFragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.playlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }
}
