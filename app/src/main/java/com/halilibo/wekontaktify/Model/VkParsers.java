package com.halilibo.wekontaktify.Model;

import com.google.gson.Gson;
import com.vk.sdk.api.VKParser;
import com.vk.sdk.api.model.VKApiAudio;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by absolute on 16.08.2014.
 */
public class VkParsers {
    public static VKParser myAudioSearchParser = new VKParser() {
        @Override
        public Object createModel(JSONObject object) {
            VkAudioResponse response = new Gson().fromJson(object.toString(), VkAudioResponse.class);
            return response.response.items;
        }
    };
    public static VKParser myAudioPopularParser = new VKParser() {
        @Override
        public Object createModel(JSONObject object) {
            VkAudioSearchResponse response = new Gson().fromJson(object.toString(), VkAudioSearchResponse.class);
            return response.response;
        }
    };
    public static VKParser myLyricsParser = new VKParser() {
        @Override
        public Object createModel(JSONObject object) {
            VkLyricsObject response = new Gson().fromJson(object.toString(), VkLyricsObject.class);
            return response.response;
        }
    };

    public class VkAudioResponse{
        public VkAudioResponseInner response;

        public class VkAudioResponseInner{
            public int count;
            public ArrayList<VKApiAudio> items;
        }
    }

    public class VkAudioSearchResponse{
        public ArrayList<VKApiAudio> response;
    }
}
