package com.halilibo.wekontaktify.Model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by absolute on 31.08.2014.
 */
public class DatabaseHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "WE_KONTAKTIFY";
    // Books table name
    private static final String TABLE_PLAYLISTS = "playlists";
    private static final String TABLE_AUDIOS = "audios";

    // Books Table Columns names
    public static final String localaudios = "All Music";

    public static final String ACTION_DATABASE = "com.halilibo.wekontaktify.action.DATABASE";
    public static final String DATABASE_UPDATE = "com.halilibo.wekontaktify.extra.DATABASE_UPDATE";
    public static final String PLAYLISTS_UPDATE = "com.halilibo.wekontaktify.extra.PLAYLISTS_UPDATE";
    private final Context context;
    private final SharedPreferences prefs;
    private final Gson gson;

    public DatabaseHelper(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences("wekontaktify", Context.MODE_PRIVATE);
        this.gson = new Gson();
        this.onCreate();
    }

    public void onCreate() {
        if(prefs.getString("playlists","").equals("")){
            ArrayList<PlaylistDataObject> playlists = new ArrayList<PlaylistDataObject>();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("playlists", gson.toJson(playlists));
            editor.commit();
        }

        if(prefs.getString("audio_ids","").equals("")){
            ArrayList<Integer> audioIds = new ArrayList<Integer>();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("playlists", gson.toJson(audioIds));
            editor.commit();
        }

    }

    public SharedPreferences getPrefs(){
        return prefs;
    }

    public ArrayList<PlaylistDataObject> getPlaylistsData(){
        String jsonPlaylists = prefs.getString("playlists","");

        ArrayList<PlaylistDataObject> playlists = gson.fromJson(jsonPlaylists, new TypeToken<ArrayList<PlaylistDataObject>>(){}.getType());

        return playlists;
    }

    public ArrayList<String> getPlaylistsTitles(){
        String jsonPlaylists = prefs.getString("playlists","");

        ArrayList<PlaylistDataObject> playlists = gson.fromJson(jsonPlaylists, new TypeToken<ArrayList<PlaylistDataObject>>(){}.getType());

        ArrayList<String> result = new ArrayList<String>();
        for(PlaylistDataObject playlist : playlists){
            result.add(playlist.title);
        }
        return result;
    }

    public Playlist getPlaylist(String title){
        Playlist result = new Playlist();
        result.title = title;

        ArrayList<PlaylistDataObject> playlists = this.getPlaylistsData();
        for(PlaylistDataObject playlist : playlists){
            if(playlist.title.equals(title)){
                result.audios = getAudiosFromIds(playlist.songIds);
                return result;
            }
        }
        return null;
    }

    public PlaylistDataObject getPlaylistDataObject(String title){
        ArrayList<PlaylistDataObject> playlists = getPlaylistsData();
        for(PlaylistDataObject object : playlists){
            if(object.title.equals(title))
                return object;
        }
        return null;
    }

    public boolean createNewPlaylist(int index, String title){
        ArrayList<PlaylistDataObject> playlists = this.getPlaylistsData();
        PlaylistDataObject newPlaylist = new PlaylistDataObject();

        for(int i=0 ; i<playlists.size(); i++){
            if(playlists.get(i).title.equals(title))
                return false;
        }

        newPlaylist.title = title;
        newPlaylist.songIds = new ArrayList<Integer>();

        playlists.add(index, newPlaylist);
        updatePlaylists(playlists);
        return true;
    }

    public void updatePlaylists(ArrayList<PlaylistDataObject> playlists){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("playlists", gson.toJson(playlists));
        editor.apply();
    }

    public ArrayList<Audio> getAudiosFromIds(ArrayList<Integer> idsArray){
        ArrayList<Audio> result = new ArrayList<Audio>();

        for(int songId : idsArray){
            String audioString = prefs.getString("audio_" + songId, "");
            Audio audio = gson.fromJson(audioString, Audio.class);
            result.add(audio);
        }
        return result;
    }

    public Audio getAudioFromId(int id){
        String audioString = prefs.getString("audio_" + id, "");
        return gson.fromJson(audioString, Audio.class);
    }

    public boolean createAudio(Audio a){
        ArrayList<Integer> audioIds = getAllAudioIds();

        if(audioIds.indexOf((Integer)a.song_id)<0){
            audioIds.add(a.song_id);
            updateAllAudioIds(audioIds);
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("audio_" + a.song_id, gson.toJson(a));
        editor.commit();
        return true;
    }

    private void updateAllAudioIds(ArrayList<Integer> audioIds) {
        String jsonAudioIds = gson.toJson(audioIds);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("audio_ids", jsonAudioIds);
        editor.commit();
    }

    public boolean updateAllAudios(ArrayList<Audio> songs){
        ArrayList<Integer> audioIds = getAllAudioIds();
        SharedPreferences.Editor editor = prefs.edit();
        for(Audio song : songs){
            if(audioIds.indexOf((Integer)song.song_id)<0){
                audioIds.add(song.song_id);
            }

            editor.putString("audio_" + song.song_id, gson.toJson(song));
            //createAudio(song);
        }

        updateAllAudioIds(audioIds);
        editor.commit();

        return true;
    }

    private ArrayList<Integer> getIdsFromAudios(ArrayList<Audio> songs) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(Audio song : songs){
            result.add(song.song_id);
        }
        return result;
    }

    public ArrayList<Audio> getDownloadedAudios(){
        ArrayList<Audio> allAudios = getAllAudios();
        ArrayList<Audio> result = new ArrayList<Audio>();
        for(int i=0; i<allAudios.size();i++){
            if(allAudios.get(i).isVk && allAudios.get(i).isOffline){
                result.add(allAudios.get(i));
            }
        }
        return result;
    }

    public ArrayList<Integer> getAllAudioIds(){
        String audioIds = prefs.getString("audio_ids","");

        ArrayList<Integer> ids = gson.fromJson(audioIds, new TypeToken<ArrayList<Integer>>(){}.getType());
        if(ids==null){
            ids = new ArrayList<Integer>();
        }

        return ids;
    }

    public ArrayList<Audio> getAllAudios(){
        return getAudiosFromIds(getAllAudioIds());
    }

    public Playlist getAllAudiosPlaylist(){
        Playlist p = new Playlist();
        p.title = localaudios;
        p.audios = getAllAudios();
        return p;
    }

    public boolean addAudioToPlaylist(int songId, String title){
        if(isAudioInPlaylist(songId, title))
            return false;
        PlaylistDataObject dataObject = getPlaylistDataObject(title);
        dataObject.songIds.add(songId);
        updatePlaylistDataObject(dataObject);
        return true;
    }

    private void updatePlaylistDataObject(PlaylistDataObject dataObject) {
        ArrayList<PlaylistDataObject> playlists = getPlaylistsData();
        for(PlaylistDataObject object : playlists){
            if(object.title.equals(dataObject.title)){
                object.songIds = dataObject.songIds;
            }
        }
        updatePlaylists(playlists);
    }

    public void removeAudioFromPlaylist(int song_id, String title) {
        PlaylistDataObject playlist = getPlaylistDataObject(title);
        for(int i=0 ; i<playlist.songIds.size() ; i++){
            if(playlist.songIds.get(i) == song_id){
                playlist.songIds.remove(i);
            }
        }
        updatePlaylistDataObject(playlist);
    }

    public boolean isAudioInPlaylist(int song_id, String title){
        PlaylistDataObject playlist = getPlaylistDataObject(title);
        for(int id:playlist.songIds){
            if(id == song_id)
                return true;
        }
        return false;
    }

    public boolean isAudioExists(int song_id){
        ArrayList<Integer> audioIds = getAllAudioIds();

        if(audioIds.indexOf((Integer)song_id)>=0)
            return true;
        return false;
    }

    public ArrayList<Audio> getOnlineAudios(){
        ArrayList<Audio> audios = getAllAudios();
        for(int i=0;i<audios.size();i++){
            if(audios.get(i).isOffline){
                audios.remove(i);
                i--;
            }

        }
        return audios;
    }

    public boolean updateAudio(Audio audio){
        if(isAudioExists(audio.song_id)){
            String jsonAudio = audio.toJson();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("audio_" + audio.song_id, jsonAudio);
            editor.commit();
            return true;
        }
        return false;
    }

    public boolean isShuffle(){
        return prefs.getBoolean("isShuffle", false);
    }

    public void setShuffle(boolean shuffle){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isShuffle", shuffle);
        editor.commit();
    }

    public int getRepeatMode(){
        return prefs.getInt("repeatMode", MediaPlayerClass.REPEAT_NONE);
    }

    public void setRepeatMode(int mode){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("repeatMode", mode);
        editor.commit();
    }

    public void sendUpdateActivity(String fragment){
        Intent intent = new Intent(ACTION_DATABASE);
        intent.putExtra(DATABASE_UPDATE, fragment);
        context.sendBroadcast(intent);
    }

    public void removeAudioCompletely(Audio audio) {
        File file = new File(audio.source);
        boolean deleted = file.delete();

        MediaScannerConnection.scanFile(
                context,
                new String[]{audio.source},
                new String[]{"audio/mp3", "*/*"},
                new MediaScannerConnection.MediaScannerConnectionClient() {
                    public void onMediaScannerConnected() {
                    }

                    public void onScanCompleted(String path, Uri uri) {
                    }
                });

        ArrayList<Integer> audio_ids = getAllAudioIds();
        if(audio_ids.indexOf(audio.song_id) >= 0)
            audio_ids.remove( audio_ids.indexOf(audio.song_id) );

        updateAllAudioIds(audio_ids);

        ArrayList<PlaylistDataObject> playlists = getPlaylistsData();
        for(PlaylistDataObject playlist:playlists){
            if(playlist.songIds.indexOf(audio.song_id)>=0)
                playlist.songIds.remove(playlist.songIds.indexOf(audio.song_id));
            updatePlaylistDataObject(playlist);
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("audio_" + audio.song_id, "");
        editor.commit();

        sendUpdateActivity(PLAYLISTS_UPDATE);
    }

    public void removeAudio(Audio audio) {
        ArrayList<Integer> audio_ids = getAllAudioIds();
        if(audio_ids.indexOf(audio.song_id) >= 0)
            audio_ids.remove( audio_ids.indexOf(audio.song_id) );

        updateAllAudioIds(audio_ids);

        ArrayList<PlaylistDataObject> playlists = getPlaylistsData();
        for(PlaylistDataObject playlist:playlists){
            if(playlist.songIds.indexOf(audio.song_id)>=0)
                playlist.songIds.remove(playlist.songIds.indexOf(audio.song_id));
            updatePlaylistDataObject(playlist);
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("audio_" + audio.song_id, "");
        editor.commit();
    }

    public void setOfflineFolder(String s){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("offline_folder", s);
        editor.commit();
    }

    public String getOfflineFolder(){
        return prefs.getString("offline_folder", "/VKontaktify");
    }

    public void updateLyrics(Audio audio, String string){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lyric_" + audio.song_id, string);
        editor.commit();
    }

    public String getLyrics(Audio audio){
        String result = prefs.getString("lyric_" + audio.song_id, "");
        return result;
    }

    public void saveMediaPlayer(MediaPlayerClass.MediaPlayerData data) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("mediaplayerdata", gson.toJson(data));
        editor.apply();
    }

    public MediaPlayerClass.MediaPlayerData getMediaPlayer(){
        return gson.fromJson(prefs.getString("mediaplayerdata", ""), MediaPlayerClass.MediaPlayerData.class);
    }
}
