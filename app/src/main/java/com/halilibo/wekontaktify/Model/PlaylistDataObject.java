package com.halilibo.wekontaktify.Model;

import java.util.ArrayList;

public class PlaylistDataObject{
    public String title;
    public ArrayList<Integer> songIds;
}