package com.halilibo.wekontaktify.Model;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by absolute on 29.08.2014.
 */
public class Playlist {
    public int id;
    public ArrayList<Audio> audios = new ArrayList<Audio>();
    public String title;

    public String toJson(){
        return new Gson().toJson(this);
    }

    public static Playlist fromJson(String string){
        return new Gson().fromJson(string, Playlist.class);
    }

    public boolean doesContain(int song_id){
        for(Audio audio: audios){
            if(audio.song_id == song_id)
                return true;
        }
        return false;
    }

    public String getSongIdsString(){
        int[] song_ids = new int[audios.size()];
        for(int i=0 ; i<audios.size() ; i++){
            song_ids[i] = audios.get(i).song_id;
        }
        return Arrays.toString(song_ids).substring(1,Arrays.toString(song_ids).length()-1);
    }

    public void removeAudio(Audio audio){
        for(int i=0;i<audios.size();i++){
            if(audio.song_id == audios.get(i).song_id){
                audios.remove(i);
                return;
            }
        }
    }


}
