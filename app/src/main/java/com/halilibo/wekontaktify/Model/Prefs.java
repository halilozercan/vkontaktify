package com.halilibo.wekontaktify.Model;

import com.vk.sdk.VKScope;

/**
 * Created by absolute on 01.09.2014.
 */
public class Prefs {
    public static final String appId = "4532338";
    public static final String[] scope = new String[] {
            VKScope.AUDIO,
            VKScope.VIDEO,
            VKScope.PHOTOS,
            VKScope.NOHTTPS
    };
}
