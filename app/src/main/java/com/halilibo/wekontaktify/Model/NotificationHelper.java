package com.halilibo.wekontaktify.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;

import java.io.InputStream;

/**
 * Created by absolute on 24.09.2014.
 */
public class NotificationHelper {
    public static Drawable getAlbumArtDrawable(Context context){
        Drawable yourDrawable;
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(Uri.parse(MediaPlayerClass.instance.findPlayingSong().albumArtUri));
            yourDrawable = Drawable.createFromStream(inputStream, MediaPlayerClass.instance.findPlayingSong().albumArtUri);
        } catch (Exception e) {
            yourDrawable = context.getResources().getDrawable(R.drawable.ic_launcher);
        }
        return yourDrawable;
    }

    public static Bitmap getAlbumArtBitmap(Context context){
        return drawableToBitmap(getAlbumArtDrawable(context));
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}
