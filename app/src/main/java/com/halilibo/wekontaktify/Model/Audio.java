package com.halilibo.wekontaktify.Model;

import android.content.Context;
import android.net.Uri;

import com.google.gson.Gson;
import com.halilibo.wekontaktify.R;
import com.vk.sdk.api.model.VKApiAudio;

import java.util.ArrayList;

/**
 * Created by absolute on 29.08.2014.
 */
public class Audio {
    public int song_id;
    public String title;
    public String artistName;
    public int artistId;
    public String albumName;
    public int albumId;
    public int duration;
    public String source;
    public int lyrics_id;
    public String genre;
    public boolean isVk;
    public boolean isOffline;
    public int vkOwnerId;
    public String albumArtUri;

    public static Audio createFromVkAudio(VKApiAudio vkAudio, Context context){
        Audio audio = new Audio();
        audio.song_id = vkAudio.id;
        audio.title = vkAudio.title;
        audio.artistName = vkAudio.artist;
        audio.duration = vkAudio.duration*1000;
        audio.source = vkAudio.url;
        audio.genre = getVkGenre(vkAudio.genre, context);
        audio.lyrics_id = vkAudio.lyrics_id;
        audio.isOffline = isOfflineContent(audio.song_id, context);
        audio.isVk = true;
        audio.vkOwnerId = vkAudio.owner_id;
        return audio;
    }

    public String toJson(){
        return new Gson().toJson(this);
    }

    public static Audio fromJson(String jsonAudio){
        return new Gson().fromJson(jsonAudio, Audio.class);
    }

    public static ArrayList<Audio> createFromVkAudios(ArrayList<VKApiAudio> vkAudios, Context context){
        ArrayList<Audio> audios = new ArrayList<Audio>();
        for(VKApiAudio vkAudio : vkAudios){
            audios.add(createFromVkAudio(vkAudio, context));
        }
        return audios;
    }

    public static String getVkGenre(int genre_id, Context context){
        return context.getResources().getStringArray(R.array.genres)[genre_id];
    }

    public static boolean isOfflineContent(int vk_id, Context context){
        DatabaseHelper db = new DatabaseHelper(context);
        Audio a = db.getAudioFromId(vk_id);
        if(a!=null){
            if(a.isVk && a.isOffline){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object a){
        if(((Audio)a).song_id == this.song_id)
            return true;
        return false;
    }

}
