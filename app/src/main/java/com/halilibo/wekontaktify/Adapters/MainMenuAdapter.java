package com.halilibo.wekontaktify.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Prefs;
import com.halilibo.wekontaktify.R;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by absolute on 07.09.2014.
 */
public class MainMenuAdapter extends BaseAdapter{

    private final DatabaseHelper db;
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;

    public MainMenuAdapter(Context con){
        super();
        this.context = con;
        db = new DatabaseHelper(con);
    }

    @Override
    public int getCount() {
        if(list==null)
            return 0;
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds(){
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = View.inflate(context, R.layout.menu_item_view, null);

        final ImageView menuItemIcon = ((ImageView) convertView.findViewById(R.id.menu_item_icon));
        final TextView menuItemText = ((TextView) convertView.findViewById(R.id.menu_item_text));

        final ImageView vkProfileImage = (ImageView) convertView.findViewById(R.id.vk_profile_image);
        final TextView vkProfileName = (TextView) convertView.findViewById(R.id.vk_profile_name);
        final RelativeLayout vkLayout = (RelativeLayout) convertView.findViewById(R.id.vk_layout);

        menuItemText.setText(list.get(position));
        switch(position){
            case 0:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.playlists));
                break;
            case 1:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.tracks));
                break;
            case 2:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.folders));
                break;
            case 3:
                if (VKSdk.wakeUpSession() || VKSdk.isLoggedIn()) {
                    menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.vk));
                    menuItemText.setText(context.getResources().getString(R.string.logged_in));
                }
                else{
                    menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.vk));
                    menuItemText.setText(context.getResources().getString(R.string.login_to_vk));
                }

                break;
        }

        return convertView;
    }
    public void setList(ArrayList<String> result) {
        list.clear();
        for(int i=0;i<result.size();i++)
            list.add(result.get(i));
        notifyDataSetChanged();
    }
}
