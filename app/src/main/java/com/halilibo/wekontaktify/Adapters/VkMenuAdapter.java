package com.halilibo.wekontaktify.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.R;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by absolute on 07.09.2014.
 */
public class VkMenuAdapter extends BaseAdapter{

    private final DatabaseHelper db;
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;

    public VkMenuAdapter(Context con){
        super();
        this.context = con;
        db = new DatabaseHelper(con);
    }

    @Override
    public int getCount() {
        if(list==null)
            return 0;
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position){
        if(position == 0){
            return false;
        }
        else
            return true;
    }

    @Override
    public boolean hasStableIds(){
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = View.inflate(context, R.layout.menu_item_view, null);

        final ImageView menuItemIcon = ((ImageView) convertView.findViewById(R.id.menu_item_icon));
        final TextView menuItemText = ((TextView) convertView.findViewById(R.id.menu_item_text));

        final ImageView vkProfileImage = (ImageView) convertView.findViewById(R.id.vk_profile_image);
        final TextView vkProfileName = (TextView) convertView.findViewById(R.id.vk_profile_name);
        final RelativeLayout vkLayout = (RelativeLayout) convertView.findViewById(R.id.vk_layout);

        menuItemText.setText(list.get(position));
        switch(position){
            case 0:
                if (VKSdk.wakeUpSession() || VKSdk.isLoggedIn()) {
                    VKRequest req = VKApi.users().get(VKParameters.from("fields", "photo_100"));
                    req.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            Log.d("mainmenuadapter", response.responseString);
                            try {
                                JSONArray results = response.json.getJSONArray("response");
                                JSONObject result = results.getJSONObject(0);
                                String firstName = result.getString("first_name").replaceAll("-", " ");
                                String lastName  = result.getString("last_name").replaceAll("-", " ");
                                String photo     = result.getString("photo_100");
                                Log.d("mainmenuadapter", firstName + " " + photo);

                                vkProfileName.setText(firstName + " " + lastName);
                                new DownloadImageTask(vkProfileImage).execute(photo);

                                vkLayout.setVisibility(View.VISIBLE);

                                menuItemIcon.setVisibility(View.GONE);
                                menuItemText.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            super.onComplete(response);
                        }

                        @Override
                        public void onError(VKError error) {
                            super.onError(error);
                        }
                    });

                }
                else{
                    menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.vk));
                    menuItemText.setText(context.getResources().getString(R.string.login_to_vk));
                }
                break;
            case 1:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.search));
                break;
            case 2:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.my_music));
                break;
            case 3:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.folders));
                break;
            case 4:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.recommendation));
                break;
            case 5:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.populars));
                break;
            case 6:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.download));
                break;
            case 7:
                menuItemIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.folder));
                TextView smallText = (TextView) convertView.findViewById(R.id.menu_item_second_text);
                smallText.setText(db.getOfflineFolder());
                smallText.setVisibility(View.VISIBLE);
                break;
        }

        return convertView;
    }
    public void setList(ArrayList<String> result) {
        list.clear();
        for(int i=0;i<result.size();i++)
            list.add(result.get(i));
        notifyDataSetChanged();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
