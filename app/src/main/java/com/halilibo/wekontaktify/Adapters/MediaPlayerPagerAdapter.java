package com.halilibo.wekontaktify.Adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.halilibo.wekontaktify.Fragments.LyricsFragment;
import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;

/**
 * Created by absolute on 04.10.2014.
 */
public class MediaPlayerPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 2;
    private final Context context;
    private final DatabaseHelper db;
    private LyricsFragment lyricsFragment;
    private PlaylistFragment queueFragment;

    public MediaPlayerPagerAdapter(Context con, FragmentManager fm){
        super(fm);
        this.context = con;
        db = new DatabaseHelper(con);

        lyricsFragment = LyricsFragment.newInstance(MediaPlayerClass.instance.findPlayingSong().song_id);
        queueFragment = PlaylistFragment.newInstance("queue");
    }

    public void searchLyricsVk(){
        lyricsFragment.searchLyricsVk();
    }

    public void searchLyricsWeb(){
        lyricsFragment.searchLyricsWeb();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getStringArray(R.array.media_player_tabs)[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return lyricsFragment;
            case 1:
                return queueFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    public void selected(int position) {
        switch(position){
            case 0:
                break;
            case 1:
                queueFragment.scrollToPlaying();
                break;
        }
    }
}
