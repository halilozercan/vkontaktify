package com.halilibo.wekontaktify.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.R;
import com.nhaarman.listviewanimations.util.Swappable;

import java.util.ArrayList;

/**
 * Created by absolute on 30.08.2014.
 */
public class UserPlaylistsAdapter extends BaseAdapter{
    private final DatabaseHelper db;
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;


    public UserPlaylistsAdapter(Context con){
        super();
        this.context = con;
        db = new DatabaseHelper(con);
    }

    @Override
    public int getCount() {
        if(list==null)
            return 0;
        if(list.size()==0)
            return 1;
        return list.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds(){
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(position == 0){
            final View rootView = View.inflate(context, R.layout.item_view_create_playlist, null);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_create_playlist);

                final EditText editText = (EditText) dialog.findViewById(R.id.create_playlist_text);
                final Button createButton = (Button) dialog.findViewById(R.id.create_button);
                Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                createButton.setEnabled(false);

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                        if(charSequence.toString().length()==0){
                            createButton.setEnabled(false);
                        }
                        else
                            createButton.setEnabled(true);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                createButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(insert(0, editText.getText().toString()))
                            dialog.dismiss();
                    }
                });

                dialog.setCancelable(true);
                dialog.setTitle(context.getResources().getString(R.string.create_playlist));

                dialog.show();
                }
            });
            return rootView;
        }
        else{
            position--;
            convertView = View.inflate(context, R.layout.playlist_item_view,null);
        }

        ((TextView) convertView.findViewById(R.id.menu_item_text)).setText(list.get(position));
        ((TextView) convertView.findViewById(R.id.playlist_item_artist_text))
                .setText(db.getPlaylistDataObject(list.get(position)).songIds.size() + " Songs");
        ((TextView) convertView.findViewById(R.id.playlist_item_duration_text)).setVisibility(View.GONE);
        convertView.findViewById(R.id.playlist_item_options_button).setVisibility(View.INVISIBLE);
        return convertView;
    }
    public void setList(ArrayList<String> result) {
        list.clear();
        for(int i=0;i<result.size();i++)
            list.add(result.get(i));
        notifyDataSetChanged();
    }

    public boolean insert(int i, String s) {
        if(db.createNewPlaylist( i ,s )){
            list.add(i, s);
            notifyDataSetChanged();
            return true;
        }
        else{
            Toast.makeText(context, context.getResources().getString(R.string.playlist_duplicate), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
