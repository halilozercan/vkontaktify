package com.halilibo.wekontaktify.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.halilibo.wekontaktify.Fragments.PlaylistFragment;
import com.halilibo.wekontaktify.Fragments.VkFragment;
import com.halilibo.wekontaktify.Model.Audio;
import com.halilibo.wekontaktify.Model.DatabaseHelper;
import com.halilibo.wekontaktify.Model.Playlist;
import com.halilibo.wekontaktify.R;
import com.halilibo.wekontaktify.Services.DownloadService;
import com.halilibo.wekontaktify.Services.MediaPlayerClass;
import com.halilibo.wekontaktify.Services.MediaPlayerService;
import com.nhaarman.listviewanimations.util.Insertable;
import com.nhaarman.listviewanimations.util.Swappable;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaylistAdapter extends BaseAdapter implements Swappable, Filterable, Insertable {

    public static final CharSequence ONLY_OFFLINE = "ONLY_OFFLINE";
    public static final CharSequence NOT_ONLY_OFFLINE = "NOT_ONLY_OFFLINE";
    public static final String SORTING = "SORTING ";

    public static final int ALL_TRACKS = 1;
    public static final int PLAYLIST = 2;
    public static final int VK_MY_MUSIC = 3;
    public static final int VK_SEARCH = 4;
    public static final int AUDIOS = 5;
    private static final int DISK = 6;
    public static final int QUEUE = 7;
    public static final int DOWNLOADED_SONGS = 8;

    private final DatabaseHelper db;
    private ArrayList<Audio> tempList = new ArrayList<Audio>();
    private ArrayList<Audio> list = new ArrayList<Audio>();
    private ArrayList<Integer> selectedIndexes = new ArrayList<Integer>();

    private Context context;
    private PlaylistFilterByText textFilter;
    private String filterByText = "";
    private boolean filterOffline = false;
    private int sorting = -1;
    public int source;
    public String playlistTitle;
    private boolean multiSelectMode = false;

        /*
    * 0 : A-Z
    * 1 : Z-A
    * 2 : Recently Added
    * */

    public PlaylistAdapter(Context con){
        super();
        this.context = con;
        db = new DatabaseHelper(con);
    }

    @Override
    public int getCount() {
        if(list==null)
            return 0;
        if(list.size()==0){
            return 1;//for empty list view
        }
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public boolean hasStableIds(){
        return true;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder{
        public TextView menuItemText;
        public TextView artistText;
        public TextView durationText;
        public ImageView optionsButton;
        public ImageView isVkView;
        public ImageView isOnlineView;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(list.size()==0) {
            convertView = View.inflate(context, R.layout.playlist_item_view_empty, null);
            return convertView;
        }
        if(convertView==null || convertView.getTag()==null){
            convertView = View.inflate(context, R.layout.playlist_item_view, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(multiSelectMode && selectedIndexes.indexOf(position)>=0){
            convertView.setBackgroundColor(context.getResources().getColor(R.color.list_selected_color));
        }else{
            convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_view_background));
        }

        if(multiSelectMode){
            holder.optionsButton.setVisibility(View.INVISIBLE);
        }else{
            holder.optionsButton.setVisibility(View.VISIBLE);
        }

        holder.menuItemText.setText(list.get(position).title);
        holder.artistText.setText(list.get(position).artistName);
        holder.durationText.setText(secsToHHMMSS(list.get(position).duration/1000));

        if(MediaPlayerClass.instance.isLoaded() && MediaPlayerClass.instance.findPlayingSong().song_id == list.get(position).song_id){
            holder.menuItemText.setTextColor(context.getResources().getColor(R.color.cancel_button_textcolor));
            holder.artistText.setTextColor(context.getResources().getColor(R.color.cancel_button_textcolor));
        }
        else{
            holder.menuItemText.setTextColor(context.getResources().getColor(R.color.black_color));
            holder.artistText.setTextColor(context.getResources().getColor(R.color.black_color));
        }

        holder.optionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createOptionsMenu(list.get(position), position, view);
            }
        });

        holder.isVkView.setImageDrawable(context.getResources().getDrawable(R.drawable.vk_active));
        holder.isOnlineView.setImageDrawable(context.getResources().getDrawable(R.drawable.online));

        if(list.get(position).isVk){
            holder.isOnlineView.setVisibility(View.VISIBLE);
            if(!list.get(position).isOffline){
                holder.isOnlineView.setVisibility(View.VISIBLE);
            }
            else
                holder.isOnlineView.setVisibility(View.GONE);
        }
        else{
            holder.isVkView.setVisibility(View.GONE);
            holder.isOnlineView.setVisibility(View.GONE);
        }
        return convertView;
    }

    private ViewHolder createViewHolder(View convertView) {
        ViewHolder holder = new ViewHolder();
        holder.menuItemText = (TextView) convertView.findViewById(R.id.menu_item_text);
        holder.artistText = (TextView) convertView.findViewById(R.id.playlist_item_artist_text);
        holder.durationText = (TextView) convertView.findViewById(R.id.playlist_item_duration_text);
        holder.optionsButton = (ImageView) convertView.findViewById(R.id.playlist_item_options_button);
        holder.isVkView = (ImageView) convertView.findViewById(R.id.playlist_item_is_vk_image);
        holder.isOnlineView = (ImageView) convertView.findViewById(R.id.playlist_item_is_online);
        return holder;
    }

    public void doneMultiSelect(View view){
        PopupMenu myMenu = new PopupMenu(context, view);
        myMenu.inflate(R.menu.options_playlist_multiselect);

        myMenu.getMenu().removeItem(R.id.options_playlist_add_to_vk);
        myMenu.getMenu().removeItem(R.id.options_playlist_remove);
        if(source == VK_SEARCH){
            myMenu.getMenu().removeItem(R.id.options_playlist_remove);
        }

        final ArrayList<Audio> songs = new ArrayList<Audio>();
        for(int index:selectedIndexes){
            songs.add(list.get(index));
        }

        myMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.options_playlist_play:
                        Playlist queue = new Playlist();
                        queue.title = "selected";
                        queue.audios.addAll(songs);
                        MediaPlayerService.startPlaylist(context, queue, 0);
                        break;
                    case R.id.options_playlist_add:
                        addToPlaylist(songs.toArray(new Audio[songs.size()]));
                        break;
                    case R.id.options_playlist_make_offline:
                        for(Audio song:songs)
                            DownloadService.startDownloadOne(context, song);
                        break;
                    case R.id.options_playlist_make_online:
                        makeOnline(songs.toArray(new Audio[songs.size()]));
                        break;
                    case R.id.options_playlist_remove:
                        /*if (source == ALL_TRACKS) {
                            if (audio.isOffline) {
                                removeFrom(audio, DISK);
                            } else {
                                removeFrom(audio, AUDIOS);
                            }
                        } else if (source == PLAYLIST) {
                            removeFrom(audio, PLAYLIST);
                        } else if (source == VK_MY_MUSIC) {
                            removeFrom(audio, VK_MY_MUSIC);//work on this
                        }*/
                        break;
                }
                exitMultiSelectMode();
                return true;
            }
        });

        myMenu.show();

    }

    public boolean isMultiSelect(){
        return multiSelectMode;
    }

    public void multiSelectItem(int position) {
        if (selectedIndexes.indexOf(position) >= 0) {
            Log.d("multiselectitem", "position: " + position);
            selectedIndexes.remove(selectedIndexes.indexOf(position));
            if (selectedIndexes.size() == 0)
                exitMultiSelectMode();
        } else {
            selectedIndexes.add(position);
        }
        notifyDataSetInvalidated();
    }

    public void enterMultiSelectMode(int position) {
        multiSelectMode = true;
        selectedIndexes.clear();
        selectedIndexes.add(position);

        Intent intent = new Intent(PlaylistFragment.ACTION_PLAYLIST_FRAGMENT);
        intent.putExtra(PlaylistFragment.PLAYLIST_UPDATE, PlaylistFragment.ENTER_MULTI_SELECT_MODE);
        context.sendBroadcast(intent);

        Intent intent2 = new Intent(VkFragment.ACTION_VK_FRAGMENT);
        intent2.putExtra(VkFragment.VK_MULTI_SELECT, VkFragment.ENTER_MULTI_SELECT_MODE);
        context.sendBroadcast(intent2);

        notifyDataSetInvalidated();
    }

    public void exitMultiSelectMode(){
        multiSelectMode = false;
        selectedIndexes.clear();

        Intent intent = new Intent(PlaylistFragment.ACTION_PLAYLIST_FRAGMENT);
        intent.putExtra(PlaylistFragment.PLAYLIST_UPDATE, PlaylistFragment.EXIT_MULTI_SELECT_MODE);
        context.sendBroadcast(intent);

        Intent intent2 = new Intent(VkFragment.ACTION_VK_FRAGMENT);
        intent2.putExtra(VkFragment.VK_MULTI_SELECT, VkFragment.EXIT_MULTI_SELECT_MODE);
        context.sendBroadcast(intent2);

        notifyDataSetInvalidated();
    }

    public void remove(Audio song){
        int id = song.song_id;
        for(int i=0;i<tempList.size();i++){
            if(id == tempList.get(i).song_id){
                tempList.remove(i);
                break;
            }
        }
        for(int i=0;i<list.size();i++){
            if(id == list.get(i).song_id){
                list.remove(i);
                break;
            }
        }
        notifyDataSetChanged();
    }

    private void createOptionsMenu(final Audio audio, final int position, View view) {

        PopupMenu myMenu = new PopupMenu(context, view);
        myMenu.inflate(R.menu.options_playlist_item);
        if(!audio.isVk){
            myMenu.getMenu().removeItem(R.id.options_playlist_make_offline);
            myMenu.getMenu().removeItem(R.id.options_playlist_make_online);
            myMenu.getMenu().removeItem(R.id.options_playlist_add_to_vk);
        }
        else{
            if(audio.isOffline)
                myMenu.getMenu().removeItem(R.id.options_playlist_make_offline);
            else
                myMenu.getMenu().removeItem(R.id.options_playlist_make_online);
        }

        if(source == VK_SEARCH){
            myMenu.getMenu().removeItem(R.id.options_playlist_remove);
        }

        myMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.options_playlist_play:
                        Playlist queue = new Playlist();
                        queue.title = "random";
                        queue.audios = getList();
                        MediaPlayerService.startPlaylist(context, queue, position);
                        break;
                    case R.id.options_playlist_add:
                        addToPlaylist(audio);
                        break;
                    case R.id.options_playlist_make_offline:
                        DownloadService.startDownloadOne(context, audio);
                        break;
                    case R.id.options_playlist_make_online:
                        makeOnline(audio);
                        break;
                    case R.id.options_playlist_add_to_vk:
                        addToVk(audio);
                        break;
                    case R.id.options_playlist_remove:
                        if (source == ALL_TRACKS || source == DOWNLOADED_SONGS) {
                            if (audio.isOffline) {
                                removeFrom(audio, DISK);
                            } else {
                                removeFrom(audio, AUDIOS);
                            }
                        } else{
                            removeFrom(audio, source);
                        }
                        break;
                }
                return true;
            }
        });

        myMenu.show();
    }

    private void removeFrom(final Audio audio, final int source) {
        if(source == QUEUE){
            remove(audio);
            MediaPlayerClass.instance.removeFromQueue(audio);
            return;
        }
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_remove_from_disk);

        TextView removeText = (TextView) dialog.findViewById(R.id.textView2);
        switch(source){
            case DISK:
                removeText.setText( audio.title + " " + context.getResources().getString(R.string.remove_from_disk_text));
                break;
            case AUDIOS:
                removeText.setText( audio.title + " " + context.getResources().getString(R.string.remove_from_audios_text));
                break;
            case PLAYLIST:
                removeText.setText( audio.title + " " + context.getResources().getString(R.string.remove_from_playlist_text));
                break;
            case VK_MY_MUSIC:
                removeText.setText( audio.title + " " + context.getResources().getString(R.string.remove_from_vkmusic_text));
                break;
        }

        Button acceptButton = (Button) dialog.findViewById(R.id.accept_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(source){
                    case DISK:
                        db.removeAudioCompletely(audio);
                        remove(audio);
                        dialog.dismiss();
                        break;
                    case AUDIOS:
                        db.removeAudio(audio);
                        remove(audio);
                        dialog.dismiss();
                        break;
                    case PLAYLIST:
                        db.removeAudioFromPlaylist(audio.song_id, playlistTitle);
                        remove(audio);
                        dialog.dismiss();
                        break;
                    case VK_MY_MUSIC:
                        VKRequest req = new VKRequest("audio.delete", VKParameters.from("audio_id", audio.song_id, "owner_id", audio.vkOwnerId));
                        req.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                Toast.makeText(context, context.getResources().getString(R.string.removed_from_vk), Toast.LENGTH_SHORT).show();
                                remove(audio);
                                dialog.dismiss();
                            }

                            @Override
                            public void onError(VKError error) {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                        break;
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.setTitle(context.getResources().getString(R.string.remove_from_disk));

        dialog.show();
    }

    private void addToVk(final Audio audio) {
        VKApi.users().get().executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
            VKApiUser user = ((VKList<VKApiUser>)response.parsedModel).get(0);
            int userid = user.id;
            if(userid == audio.vkOwnerId){
                Toast.makeText(context, context.getResources().getString(R.string.already_added_to_vk), Toast.LENGTH_SHORT).show();
            }
            else{
                VKRequest req = new VKRequest("audio.add", VKParameters.from("audio_id", audio.song_id, "owner_id", audio.vkOwnerId));
                req.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        Toast.makeText(context, audio.title + " " + context.getResources().getString(R.string.added_to_vk), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(VKError error) {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        super.onError(error);
                    }
                });
            }
            }
        });


    }

    private void makeOnline(Audio... audios) {
        for(Audio audio:audios){
            if(db.isAudioExists(audio.song_id) && audio.isOffline && audio.isVk){
                File file = new File(audio.source);
                boolean deleted = file.delete();
                if(deleted){
                    audio.isOffline = false;
                    db.updateAudio(audio);
                }
            }
        }

        DownloadService.startRenew(context);
        notifyDataSetInvalidated();
    }

    public void refreshOfflineContent(){
        for(Audio audio:list){
            audio.isOffline = Audio.isOfflineContent(audio.song_id, context);
        }
        notifyDataSetInvalidated();
    }

    public void setList(ArrayList<Audio> result) {
        list.clear();
        list.addAll(result);

        tempList.clear();
        tempList.addAll(result);


        this.getFilter().filter(filterByText);


    }
    public void remove(int index){
        list.remove(index);
        notifyDataSetChanged();
    }

    public static String secsToHHMMSS(int secs){

        int hours = secs / 3600,
                remainder = secs % 3600,
                minutes = remainder / 60,
                seconds = remainder % 60;

        String result = "";
        if(hours>=10)
            result += hours+":";
        else if(hours>0)
            result += "0" + hours + ":";
        if(minutes>=10)
            result += minutes+":";
        else if(minutes>=0)
            result += "0" + minutes + ":";
        if(seconds>=10)
            result += seconds;
        else if(seconds>=0)
            result += "0" + seconds;


        return result;

    }

    private void addToPlaylist(final Audio... songs){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_add_to_playlist);

        ListView lv = (ListView) dialog.findViewById(R.id.dialog_listview);

        final UserPlaylistsAdapter aa = new UserPlaylistsAdapter(context);
        aa.setList(db.getPlaylistsTitles());
        lv.setAdapter(aa);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for(Audio song:songs){
                    if(!db.isAudioExists(song.song_id))
                        db.createAudio(song);
                    db.addAudioToPlaylist(song.song_id, (String)aa.getItem(i - 1));
                }
                db.sendUpdateActivity(DatabaseHelper.PLAYLISTS_UPDATE);
                Toast.makeText(context, context.getResources().getString(R.string.added_to_playlist), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.setTitle(context.getResources().getString(R.string.playlists));

        dialog.show();
    }

    @Override
    public void swapItems(int i, int i2) {
        Audio a1 = list.get(i);
        Audio a2 = list.get(i2);
        list.set(i, a2);
        list.set(i2, a1);

        int index1 = findAudioInList(a1, tempList);
        int index2 = findAudioInList(a2, tempList);

        tempList.set(index1, a2);
        tempList.set(index2, a1);
    }

    public static int findAudioInList(Audio a, ArrayList<Audio> list){
        int index = -1;
        for(int i=0 ;i < list.size(); i++){
            if(list.get(i).song_id == a.song_id)
                return i;
        }
        return index;
    }

    public boolean getOfflineFilter(){
        return filterOffline;
    }

    @Override
    public Filter getFilter() {
        if (textFilter == null)
            textFilter = new PlaylistFilterByText();

        return textFilter;
    }

    public ArrayList<Audio> getList(){
        return list;
    }

    @Override
    public void add(int i, Object o) {
        list.add(i, (Audio) o);
        tempList.add(i, (Audio) o);

        notifyDataSetChanged();
    }

    private class PlaylistFilterByText extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String consStr = constraint.toString();
            if(consStr.equals(ONLY_OFFLINE))
                filterOffline = true;
            else if(consStr.equals(NOT_ONLY_OFFLINE))
                filterOffline = false;
            else if(consStr.startsWith(SORTING)){
                Matcher matcher = Pattern.compile("\\d+").matcher(constraint);
                matcher.find();
                int i = Integer.valueOf(matcher.group());
                sorting=i;
            }
            else{
                filterByText = consStr;
            }
            Log.d("filteringlist", "filterbytext: " + filterByText + " isOffline: " + filterOffline);
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            ArrayList<Audio> filteredList = new ArrayList<Audio>();
            for(Audio audio:tempList){
                filteredList.add(audio);
            }

            if(filterOffline){

                for(int i=0 ; i<filteredList.size() ; i++){
                    if ( filteredList.get(i).isVk && !filteredList.get(i).isOffline ){
                        filteredList.remove(i);
                        i--;
                    }
                }
            }
            if(filterByText.length()>0){
                for(int i=0 ; i<filteredList.size() ; i++){
                    if ( !filteredList.get(i).title.toLowerCase().contains(filterByText)
                            && !filteredList.get(i).artistName.toLowerCase().contains(filterByText)){
                        filteredList.remove(i);
                        i--;
                    }
                }
            }
            switch(sorting){
                case 0:
                    Collections.sort(filteredList, new TitleAZComparator());
                    break;
                case 1:
                    Collections.sort(filteredList, new TitleZAComparator());
                    break;
            }
            results.values = filteredList;
            results.count = filteredList.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            // Now we have to inform the adapter about the new list filtered
            /*if (filterResults.count == 0)
                notifyDataSetInvalidated();
            else {
                list = (ArrayList<Audio>) filterResults.values;
                notifyDataSetChanged();
            }*/

            list = (ArrayList<Audio>) filterResults.values;
            notifyDataSetChanged();
        }

        public class TitleAZComparator implements Comparator<Audio> {
            @Override
            public int compare(Audio a1, Audio a2) {
                return a1.title.compareTo(a2.title);
            }
        }

        public class TitleZAComparator implements Comparator<Audio> {
            @Override
            public int compare(Audio a1, Audio a2) {
                return a2.title.compareTo(a1.title);
            }
        }

    }
}